from rest_framework import serializers

from base.serializers import Base64FileField
from recipe.models import Recipe, Instruction, Ingredient, IngredientList
from recipe.serializer_methods import repr_count


class RecipeCreateSerializer(serializers.ModelSerializer):
    """Recipe Create Serializer"""
    image = Base64FileField(required=False, help_text='data:image/jpg;base64,/9j/4A...', allow_null=True)

    class Meta:
        model = Recipe
        fields = ['id', 'title', 'image', 'cooking_time', 'number_portions',
                  'complexity', 'advice', 'created', 'updated']


class RecipeListSerializer(serializers.ModelSerializer):
    """Recipe List Serializer"""

    class Meta:
        model = Recipe
        fields = ['id', 'title', 'image', 'created', 'updated']


class IngredientSerializer(serializers.ModelSerializer):
    """Ingredient Serializer"""

    class Meta:
        model = Ingredient
        fields = ['id', 'title', 'count', 'unit', 'ordering', 'created', 'updated']

    def to_representation(self, instance):
        to_representation = super().to_representation(instance)
        return repr_count(to_representation, instance)


class InstructionsSerializer(serializers.ModelSerializer):
    """Instructions Serializer"""

    class Meta:
        model = Instruction
        fields = ['id', 'title', 'ordering', 'created', 'updated']


class RecipeDetailSerializer(serializers.ModelSerializer):
    """Recipe Detail Serializer"""
    ingredients = IngredientSerializer(many=True)
    instructions = InstructionsSerializer(many=True)

    class Meta:
        model = Recipe
        fields = ['id', 'title', 'image', 'cooking_time', 'number_portions',
                  'complexity', 'advice', 'ingredients', 'instructions', 'created', 'updated']


class RecipeUpdateSerializer(serializers.ModelSerializer):
    """Recipe Update Serializer"""
    image = Base64FileField(required=False, help_text='data:image/jpg;base64,/9j/4A...', allow_null=True)

    class Meta:
        model = Recipe
        fields = ['id', 'title', 'cooking_time', 'number_portions', 'complexity', 'advice', 'image']


class IngredientListUserSerializer(serializers.ModelSerializer):
    """User's Ingredient List Serializer"""

    class Meta:
        model = IngredientList
        fields = ['title']


class IngredientCreateSerializer(serializers.ModelSerializer):
    """Ingredient Create Serializer"""
    recipe_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = Ingredient
        fields = ['id', 'recipe_id', 'title', 'count', 'unit', 'ordering']

    def to_representation(self, instance):
        to_representation = super().to_representation(instance)
        return repr_count(to_representation, instance)

    def create(self, validated_data):
        recipe_id = validated_data.get('recipe_id', None)
        if recipe_id:
            validated_data.pop('recipe_id')
        return super().create(validated_data)


class IngredientUpdateSerializer(serializers.ModelSerializer):
    """Ingredient Update Serializer"""

    class Meta:
        model = Ingredient
        fields = ['id', 'title', 'count', 'unit', 'ordering']

    def to_representation(self, instance):
        to_representation = super().to_representation(instance)
        return repr_count(to_representation, instance)


class InstructionListSerializer(serializers.ModelSerializer):
    """Instruction List Serializer"""

    class Meta:
        model = Instruction
        fields = ['id', 'title', 'ordering', 'created', 'updated']


class InstructionCreateSerializer(serializers.ModelSerializer):
    """Instruction Create Serializer"""
    recipe_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = Instruction
        fields = ['id', 'recipe_id', 'title', 'ordering', 'created', 'updated']

    def create(self, validated_data):
        recipe_id = validated_data.get('recipe_id', None)
        if recipe_id:
            validated_data.pop('recipe_id')
        return super().create(validated_data)


class InstructionUpdateSerializer(serializers.ModelSerializer):
    """Instruction Update Serializer"""

    class Meta:
        model = Instruction
        fields = ['id', 'title', 'ordering', 'created', 'updated']
