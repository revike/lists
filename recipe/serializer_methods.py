from collections import OrderedDict
from decimal import Decimal

from recipe.models import Ingredient


def repr_count(to_representation: OrderedDict, instance: Ingredient) -> OrderedDict:
    """Repr count"""
    if isinstance(instance.count, Decimal):
        to_representation['count'] = f'{float(instance.count):g}'
    return to_representation
