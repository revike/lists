from django.urls import path

from recipe.views import RecipeListApiView, RecipeDetailApiView, RecipeUpdateApiView, RecipeDeleteApiView, \
    RecipeCreateApiView, IngredientListUserApiView, IngredientCreateApiView, IngredientUpdateApiView, \
    IngredientDeleteApiView, InstructionListApiView, InstructionCreateApiView, InstructionUpdateApiView, \
    InstructionDeleteApiView

app_name = 'recipe'

urlpatterns = [
    path('create/', RecipeCreateApiView.as_view(), name='create'),
    path('list/', RecipeListApiView.as_view(), name='list'),
    path('<int:pk>/', RecipeDetailApiView.as_view(), name='detail'),
    path('update/<int:pk>/', RecipeUpdateApiView.as_view(), name='update'),
    path('delete/<int:pk>/', RecipeDeleteApiView.as_view(), name='delete'),

    path('ingredient/list/user/', IngredientListUserApiView.as_view(), name='ingredient_user'),
    path('ingredient/create/', IngredientCreateApiView.as_view(), name='ingredient_create'),
    path('ingredient/update/<int:pk>/', IngredientUpdateApiView.as_view(), name='ingredient_update'),
    path('ingredient/delete/<int:pk>/', IngredientDeleteApiView.as_view(), name='ingredient_delete'),

    path('instruction/list/<int:recipe_id>/', InstructionListApiView.as_view(), name='instruction_list'),
    path('instruction/create/', InstructionCreateApiView.as_view(), name='instruction_create'),
    path('instruction/update/<int:pk>/', InstructionUpdateApiView.as_view(), name='instruction_update'),
    path('instruction/delete/<int:pk>/', InstructionDeleteApiView.as_view(), name='instruction_delete'),
]
