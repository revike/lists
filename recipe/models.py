import os
from decimal import Decimal

from django.core.validators import MinValueValidator
from django.db import models
from django.utils.translation import gettext_lazy as _

from base.choices import UnitChoice
from base.models import BaseModel, NULLABLE
from base.services.upload_files import upload_to_image_recipe
from config.settings import MEDIA_URL
from recipe.choices import ComplexityChoice


class IngredientList(BaseModel):
    """Ingredient List"""
    title = models.CharField(_('title'), max_length=128)
    user = models.ForeignKey('users.User', on_delete=models.CASCADE,
                             related_name='ingredient_list_user', verbose_name=_('user'))

    class Meta:
        verbose_name = _('ingredient list')
        verbose_name_plural = _('ingredient lists')

    def __str__(self):
        return f'{self.title}'


class Instruction(BaseModel):
    """Instruction"""
    title = models.CharField(_('title'), max_length=1024)
    ordering = models.PositiveIntegerField(_('ordering'), **NULLABLE)
    user = models.ForeignKey('users.User', on_delete=models.CASCADE,
                             related_name='instruction_user', verbose_name=_('user'))

    class Meta:
        verbose_name = _('instruction')
        verbose_name_plural = _('instructions')
        ordering = ['ordering', 'created']

    def __str__(self):
        return f'{self.title}'


class Ingredient(BaseModel):
    """Ingredient"""
    title = models.CharField(_('title'), max_length=128)
    user = models.ForeignKey('users.User', on_delete=models.CASCADE,
                             related_name='ingredient_user', verbose_name=_('user'))
    count = models.DecimalField(_('count'), **NULLABLE, decimal_places=3, max_digits=10,
                                validators=[MinValueValidator(Decimal('0.00'))])
    unit = models.CharField(_('unit'), max_length=6, choices=UnitChoice.choices, **NULLABLE)
    ordering = models.PositiveIntegerField(_('ordering'), **NULLABLE)

    class Meta:
        verbose_name = _('ingredient')
        verbose_name_plural = _('ingredients')
        ordering = ['ordering', 'created']

    def __str__(self):
        return f'{self.title}'


class Recipe(BaseModel):
    """Recipe"""
    title = models.CharField(_('title'), max_length=128)
    user = models.ForeignKey('users.User', on_delete=models.CASCADE, related_name='recipe_user', verbose_name=_('user'))
    ingredients = models.ManyToManyField(Ingredient, blank=True,
                                         related_name='recipe_ingredients', verbose_name=_('ingredients'))
    instructions = models.ManyToManyField(Instruction, blank=True,
                                          related_name='recipe_instructions', verbose_name=_('instructions'))
    cooking_time = models.TimeField(_('cooking time'), **NULLABLE)
    number_portions = models.PositiveIntegerField(_('number of portions'), **NULLABLE)
    complexity = models.CharField(_('complexity'), max_length=32,
                                  choices=ComplexityChoice.choices, default=ComplexityChoice.EASY)
    advice = models.TextField(_('advice'), max_length=2048, **NULLABLE)
    image = models.ImageField(_('image'), upload_to=upload_to_image_recipe, **NULLABLE)
    __image = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__image = self.image

    class Meta:
        verbose_name = _('recipe')
        verbose_name_plural = _('recipes')

    def __str__(self):
        return f'{self.title}'

    def save(self, *args, **kwargs):
        if self.__image != self.image:
            file = f'{MEDIA_URL}{self.__image}'
            if os.path.isfile(f'{file}'):
                os.remove(file)
        super().save(*args, **kwargs)

    def delete(self, using=None, keep_parents=False):
        file = f'{MEDIA_URL}{self.image}'
        if os.path.isfile(f'{file}'):
            os.remove(file)
        return super().delete(using, keep_parents)
