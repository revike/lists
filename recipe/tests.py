from django.urls import reverse, resolve
from rest_framework import status

from base.tests import BaseTestCase
from recipe import views
from recipe.choices import ComplexityChoice
from recipe.models import Recipe, Ingredient, IngredientList, Instruction


class TestRecipeCase(BaseTestCase):
    """Test Recipe"""

    def setUp(self) -> None:
        super().setUp()
        self.recipe = Recipe.objects.create(user=self.user)
        self.ingredient = Ingredient.objects.create(title='title', user=self.user)
        self.recipe.ingredients.add(self.ingredient)
        self.instruction = Instruction.objects.create(user=self.user, title='instruction_title')
        self.recipe.instructions.add(self.instruction)

    def test_recipe_list(self):
        """Test recipe list"""
        recipes = Recipe.objects.filter(user=self.user)
        url, view = reverse('recipe:list'), views.RecipeListApiView
        self.make_get(url, None, status_code=status.HTTP_401_UNAUTHORIZED)
        response = self.make_get(url, self.user.email, status_code=status.HTTP_200_OK)
        self.assertEqual(len(response), recipes.count())
        url_hard = f'{url}?complexity=hard'
        response = self.make_get(url_hard, self.user.email, status_code=status.HTTP_200_OK)
        self.assertEqual(len(response), recipes.filter(complexity=ComplexityChoice.HARD).count())
        self.assertEqual(resolve(url).func.view_class, view)

    def test_recipe_detail(self):
        """Test recipe detail"""
        url, view = reverse('recipe:detail', kwargs={'pk': self.recipe.id}), views.RecipeDetailApiView
        self.make_get(url, None, status_code=status.HTTP_401_UNAUTHORIZED)
        response = self.make_get(url, self.user.email, status_code=status.HTTP_200_OK)
        self.assertEqual(response['id'], self.recipe.id)
        self.assertEqual(resolve(url).func.view_class, view)

    def test_recipe_update(self):
        """Test recipe update"""
        data = {'title': f'{self.recipe.title}_new'}
        url, view = reverse('recipe:update', kwargs={'pk': self.recipe.id}), views.RecipeUpdateApiView
        self.make_patch(url, None, data, status_code=status.HTTP_401_UNAUTHORIZED)
        response = self.make_patch(url, self.user.email, data, status_code=status.HTTP_200_OK)
        self.assertNotEqual(response['title'], self.recipe.title)
        self.assertEqual(resolve(url).func.view_class, view)

    def test_recipe_delete(self):
        """Test recipe delete"""
        url, view = reverse('recipe:delete', kwargs={'pk': self.recipe.id}), views.RecipeDeleteApiView
        self.make_delete(url, None, status_code=status.HTTP_401_UNAUTHORIZED)
        self.make_delete(url, self.user.email, status_code=status.HTTP_204_NO_CONTENT)
        recipe = Recipe.objects.filter(id=self.recipe.id).count()
        self.assertEqual(recipe, 0)
        self.assertEqual(resolve(url).func.view_class, view)

    def test_recipe_create(self):
        """Test recipe create"""
        recipe_start = Recipe.objects.all().count()
        data = {'title': 'create_recipe'}
        url, view = reverse('recipe:create'), views.RecipeCreateApiView
        self.make_post(url, None, data, status_code=status.HTTP_401_UNAUTHORIZED)
        response = self.make_post(url, self.user.email, data, status_code=status.HTTP_201_CREATED)
        self.assertEqual(response['title'], data['title'])
        recipe_finish = Recipe.objects.all().count()
        self.assertEqual(recipe_finish, recipe_start + 1)
        self.assertEqual(resolve(url).func.view_class, view)

    def test_ingredient_list_user(self):
        """Test user's ingredient list"""
        ingredients_count = IngredientList.objects.filter(user=self.user).count()
        url, view = reverse('recipe:ingredient_user'), views.IngredientListUserApiView
        self.make_get(url, None, status_code=status.HTTP_401_UNAUTHORIZED)
        response = self.make_get(url, self.user.email, status_code=status.HTTP_200_OK)
        self.assertEqual(len(response), ingredients_count)
        self.assertEqual(resolve(url).func.view_class, view)

    def test_ingredient_create(self):
        """Test ingredient create"""
        ingredients_start = Ingredient.objects.filter(user=self.user).count()
        url, view = reverse('recipe:ingredient_create'), views.IngredientCreateApiView
        data = {'title': 'new_ingredient', 'recipe_id': self.recipe.id}
        self.make_post(url, None, data, status_code=status.HTTP_401_UNAUTHORIZED)
        response = self.make_post(url, self.user.email, data, status_code=status.HTTP_201_CREATED)
        ingredients_finish = Ingredient.objects.filter(user=self.user).count()
        self.assertEqual(response['title'], data['title'])
        self.assertEqual(ingredients_start + 1, ingredients_finish)
        self.assertEqual(resolve(url).func.view_class, view)

    def test_ingredient_update(self):
        """Test ingredient update"""
        url = reverse('recipe:ingredient_update', kwargs={'pk': self.ingredient.pk})
        view = views.IngredientUpdateApiView
        data = {'title': 'update_ingredient'}
        self.make_patch(url, None, data, status_code=status.HTTP_401_UNAUTHORIZED)
        response = self.make_patch(url, self.user.email, data, status_code=status.HTTP_200_OK)
        self.assertEqual(response['title'], data['title'])
        self.assertEqual(resolve(url).func.view_class, view)

    def test_instruction_list(self):
        """Test instruction list"""
        instructions = Instruction.objects.filter(user=self.user, recipe_instructions__id=self.recipe.id)
        url = reverse('recipe:instruction_list', kwargs={'recipe_id': self.recipe.id})
        view = views.InstructionListApiView
        self.make_get(url, None, status_code=status.HTTP_401_UNAUTHORIZED)
        response = self.make_get(url, self.user.email, status_code=status.HTTP_200_OK)
        self.assertEqual(len(response), instructions.count())
        self.assertEqual(resolve(url).func.view_class, view)

    def test_instruction_create(self):
        """Test instruction create"""
        data = {'recipe_id': self.recipe.id, 'title': 'title_create_instruction'}
        instructions_start = Instruction.objects.filter(user=self.user, recipe_instructions__id=self.recipe.id).count()
        url, view = reverse('recipe:instruction_create'), views.InstructionCreateApiView
        self.make_post(url, None, data, status_code=status.HTTP_401_UNAUTHORIZED)
        response = self.make_post(url, self.user.email, data, status_code=status.HTTP_201_CREATED)
        instructions_finish = Instruction.objects.filter(user=self.user, recipe_instructions__id=self.recipe.id).count()
        self.assertEqual(response['title'], data['title'])
        self.assertEqual(instructions_start + 1, instructions_finish)
        self.assertEqual(resolve(url).func.view_class, view)

    def test_instruction_update(self):
        """Test instruction update"""
        data = {'title': 'instruction_update'}
        url = reverse('recipe:instruction_update', kwargs={'pk': self.instruction.id})
        view = views.InstructionUpdateApiView
        self.make_patch(url, None, data, status_code=status.HTTP_401_UNAUTHORIZED)
        response = self.make_patch(url, self.user.email, data, status_code=status.HTTP_200_OK)
        self.assertEqual(response['title'], data['title'])
        self.assertEqual(resolve(url).func.view_class, view)

    def test_instruction_delete(self):
        """Test instruction delete"""
        url = reverse('recipe:instruction_delete', kwargs={'pk': self.instruction.id})
        view = views.InstructionDeleteApiView
        self.make_delete(url, None, status_code=status.HTTP_401_UNAUTHORIZED)
        self.make_delete(url, self.user.email, status_code=status.HTTP_204_NO_CONTENT)
        instruction = Instruction.objects.filter(user=self.user).count()
        self.assertEqual(instruction, 0)
        self.assertEqual(resolve(url).func.view_class, view)
