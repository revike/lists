# Generated by Django 5.0.2 on 2024-06-07 07:22

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recipe', '0004_ingredientlist'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='ingredient',
            options={'ordering': ['ordering', 'created'], 'verbose_name': 'ingredient', 'verbose_name_plural': 'ingredients'},
        ),
        migrations.AlterModelOptions(
            name='ingredientlist',
            options={'verbose_name': 'ingredient list', 'verbose_name_plural': 'ingredient lists'},
        ),
        migrations.AlterModelOptions(
            name='instruction',
            options={'ordering': ['ordering', 'created'], 'verbose_name': 'instruction', 'verbose_name_plural': 'instructions'},
        ),
    ]
