from django.db import models
from django.utils.translation import gettext_lazy as _


class ComplexityChoice(models.TextChoices):
    """Complexity Choices"""
    EASY = 'easy', _('easy')
    MEDIUM = 'medium', _('medium')
    HARD = 'hard', _('hard')
