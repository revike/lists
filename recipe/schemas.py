from rest_framework import status

from base.schemas import base_responses
from recipe.serializers import RecipeListSerializer, RecipeDetailSerializer, RecipeUpdateSerializer, \
    RecipeCreateSerializer, IngredientListUserSerializer, IngredientCreateSerializer, IngredientUpdateSerializer, \
    InstructionListSerializer, InstructionCreateSerializer, InstructionUpdateSerializer

# Recipe
response_recipe_create = base_responses.copy()
response_recipe_create[status.HTTP_201_CREATED] = RecipeCreateSerializer
del response_recipe_create[status.HTTP_404_NOT_FOUND]

response_recipe_list = base_responses.copy()
response_recipe_list[status.HTTP_200_OK] = RecipeListSerializer
del response_recipe_list[status.HTTP_404_NOT_FOUND]

response_recipe_detail = base_responses.copy()
response_recipe_detail[status.HTTP_200_OK] = RecipeDetailSerializer

response_recipe_update = base_responses.copy()
response_recipe_update[status.HTTP_200_OK] = RecipeUpdateSerializer

response_recipe_delete = base_responses.copy()
response_recipe_delete[status.HTTP_204_NO_CONTENT] = None

# Ingredient
response_ingredient_list = base_responses.copy()
response_ingredient_list[status.HTTP_200_OK] = IngredientListUserSerializer
del response_ingredient_list[status.HTTP_404_NOT_FOUND]

response_ingredient_create = base_responses.copy()
response_ingredient_create[status.HTTP_201_CREATED] = IngredientCreateSerializer
del response_ingredient_create[status.HTTP_404_NOT_FOUND]

response_ingredient_update = base_responses.copy()
response_ingredient_update[status.HTTP_200_OK] = IngredientUpdateSerializer

response_ingredient_delete = base_responses.copy()
response_ingredient_delete[status.HTTP_204_NO_CONTENT] = None

# Instruction
response_instruction_list = base_responses.copy()
response_instruction_list[status.HTTP_200_OK] = InstructionListSerializer

response_instruction_create = base_responses.copy()
del response_instruction_create[status.HTTP_404_NOT_FOUND]
response_instruction_create[status.HTTP_201_CREATED] = InstructionCreateSerializer

response_instruction_update = base_responses.copy()
response_instruction_update[status.HTTP_200_OK] = InstructionUpdateSerializer

response_instruction_delete = base_responses.copy()
response_instruction_delete[status.HTTP_204_NO_CONTENT] = None
