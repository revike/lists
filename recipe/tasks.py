from celery import shared_task

from recipe.models import Ingredient, IngredientList


@shared_task(autoretry_for=(Exception,), retry_kwargs={'max_retries': 5, 'countdown': 10})
def delete_empty_ingredients(user_id: int, instance_title: str) -> None:
    """Delete empty ingredients"""
    ingredients = Ingredient.objects.filter(user_id=user_id, title=instance_title)
    if not ingredients:
        ingredient_list = IngredientList.objects.filter(user_id=user_id, title=instance_title)
        ingredient_list.delete()
