from django.contrib import admin

from recipe.models import Recipe, Instruction, Ingredient, IngredientList


@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    """Recipe Admin panel"""
    list_display = ['id', 'title', 'user', 'created', 'updated']
    list_display_links = list_display
    readonly_fields = ['id', 'created', 'updated']
    fields = ['id', 'title', 'user', 'ingredients', 'instructions', 'cooking_time', 'number_portions', 'complexity',
              'advice', 'image']
    search_fields = ['title', 'user__email']
    list_filter = ['complexity', 'created', 'updated']
    filter_horizontal = ['ingredients', 'instructions']


@admin.register(Ingredient)
class IngredientAdmin(admin.ModelAdmin):
    """Ingredient Admin panel"""
    list_display = ['id', 'title', 'count', 'unit', 'created', 'updated']
    list_display_links = list_display


@admin.register(Instruction)
class InstructionAdmin(admin.ModelAdmin):
    """Instruction Admin panel"""
    list_display = ['id', 'title', 'ordering', 'created', 'updated']
    list_display_links = list_display


@admin.register(IngredientList)
class IngredientListAdmin(admin.ModelAdmin):
    """IngredientList Admin panel"""
