from django.utils.translation import gettext_lazy as _
from drf_spectacular.utils import extend_schema
from rest_framework import generics
from rest_framework.exceptions import ValidationError
from rest_framework.generics import get_object_or_404

from base.permissions import IsAuthenticatedPermission
from config.settings import ENV_TYPE
from recipe.models import Recipe, IngredientList, Ingredient, Instruction
from recipe.schemas import response_recipe_list, response_recipe_detail, response_recipe_update, \
    response_recipe_delete, response_recipe_create, response_ingredient_list, response_ingredient_create, \
    response_ingredient_update, response_ingredient_delete, response_instruction_list, response_instruction_create, \
    response_instruction_update, response_instruction_delete
from recipe.serializers import RecipeListSerializer, RecipeDetailSerializer, RecipeUpdateSerializer, \
    RecipeCreateSerializer, IngredientListUserSerializer, IngredientCreateSerializer, IngredientUpdateSerializer, \
    InstructionListSerializer, InstructionCreateSerializer, InstructionUpdateSerializer
from recipe.tasks import delete_empty_ingredients


class RecipeCreateApiView(generics.CreateAPIView):
    """Recipe Create"""
    permission_classes = [IsAuthenticatedPermission]
    serializer_class = RecipeCreateSerializer

    @extend_schema(summary=_('Recipe Create'), responses=response_recipe_create)
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)

    def perform_create(self, serializer):
        data = serializer.validated_data
        data['user'] = self.request.user
        serializer.save()


class RecipeListApiView(generics.ListAPIView):
    """Recipe List"""
    permission_classes = [IsAuthenticatedPermission]
    serializer_class = RecipeListSerializer
    filterset_fields = ['complexity']

    @extend_schema(summary=_('Recipe List'), responses=response_recipe_list)
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        user = self.request.user
        return Recipe.objects.filter(user=user)


class RecipeDetailApiView(generics.RetrieveAPIView):
    """Recipe Detail"""
    permission_classes = [IsAuthenticatedPermission]
    serializer_class = RecipeDetailSerializer

    @extend_schema(summary=_('Recipe Detail'), responses=response_recipe_detail)
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        user = self.request.user
        return Recipe.objects.filter(user=user)


class RecipeUpdateApiView(generics.UpdateAPIView):
    """Recipe Update"""
    permission_classes = [IsAuthenticatedPermission]
    serializer_class = RecipeUpdateSerializer
    http_method_names = ['patch']

    @extend_schema(summary=_('Recipe Update'), responses=response_recipe_update)
    def patch(self, request, *args, **kwargs):
        return super().patch(request, *args, **kwargs)

    def get_queryset(self):
        user = self.request.user
        return Recipe.objects.filter(user=user)


class RecipeDeleteApiView(generics.DestroyAPIView):
    """Recipe Delete"""
    permission_classes = [IsAuthenticatedPermission]

    @extend_schema(summary=_('Recipe Delete'), responses=response_recipe_delete)
    def delete(self, request, *args, **kwargs):
        return super().delete(request, *args, **kwargs)

    def get_queryset(self):
        user = self.request.user
        return Recipe.objects.filter(user=user)

    def perform_destroy(self, instance):
        user = self.request.user
        ingredients = instance.ingredients.all()
        if ENV_TYPE in ('dev', 'prod',):
            [delete_empty_ingredients.delay(user.id, ingredient.title) for ingredient in ingredients]
        instructions = instance.instructions.all()
        ingredients.delete()
        instructions.delete()
        instance.delete()


class IngredientListUserApiView(generics.ListAPIView):
    """User's Ingredient List"""
    permission_classes = [IsAuthenticatedPermission]
    serializer_class = IngredientListUserSerializer

    @extend_schema(summary=_('Ingredient List'), responses=response_ingredient_list, tags=['ingredient'])
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        user = self.request.user
        return IngredientList.objects.filter(user=user)


class IngredientCreateApiView(generics.CreateAPIView):
    """Ingredient Create"""
    permission_classes = [IsAuthenticatedPermission]
    serializer_class = IngredientCreateSerializer

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.recipe = None

    @extend_schema(summary=_('Ingredient Create'), responses=response_ingredient_create, tags=['ingredient'])
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        user = self.request.user
        validated_data = self.request.data
        recipe_id = validated_data.get('recipe_id', None)
        error_message = {'recipe_id': [_('Required field')]}
        if recipe_id:
            self.recipe = Recipe.objects.filter(id=recipe_id, user=user).first()
            error_message['recipe_id'] = [_('There is no recipe with this id')]
            if self.recipe:
                return super().create(request, *args, **kwargs)
        raise ValidationError(error_message)

    def perform_create(self, serializer):
        data = serializer.validated_data
        user = self.request.user
        data['user'] = user
        ingredient = serializer.save()
        self.recipe.ingredients.add(ingredient)
        title = ingredient.title
        IngredientList.objects.get_or_create(title=title, user=user)


class IngredientUpdateApiView(generics.UpdateAPIView):
    """Ingredient Update"""
    permission_classes = [IsAuthenticatedPermission]
    serializer_class = IngredientUpdateSerializer
    http_method_names = ['patch']

    @extend_schema(summary=_('Ingredient Update'), responses=response_ingredient_update, tags=['ingredient'])
    def patch(self, request, *args, **kwargs):
        return super().patch(request, *args, **kwargs)

    def perform_update(self, serializer):
        instance = self.get_object()
        serializer.save()
        data = self.request.data
        if 'title' in data:
            instance_title = instance.title
            title = data['title']
            user = self.request.user
            _, create = IngredientList.objects.get_or_create(title=title, user=user)
            if create:
                if ENV_TYPE in ('dev', 'prod',):
                    delete_empty_ingredients.delay(user.id, instance_title)

    def get_queryset(self):
        user = self.request.user
        return Ingredient.objects.filter(user=user)


class IngredientDeleteApiView(generics.DestroyAPIView):
    """Ingredient Delete"""
    permission_classes = [IsAuthenticatedPermission]

    @extend_schema(summary=_('Ingredient Delete'), responses=response_ingredient_delete, tags=['ingredient'])
    def delete(self, request, *args, **kwargs):
        return super().delete(request, *args, **kwargs)

    def get_queryset(self):
        user = self.request.user
        return Ingredient.objects.filter(user=user)

    def perform_destroy(self, instance):
        instance_title = instance.title
        user = self.request.user
        if ENV_TYPE in ('dev', 'prod',):
            delete_empty_ingredients.delay(user.id, instance_title)
        instance.delete()


class InstructionListApiView(generics.ListAPIView):
    """Instruction List"""
    permission_classes = [IsAuthenticatedPermission]
    serializer_class = InstructionListSerializer

    @extend_schema(summary=_('Instruction List'), responses=response_instruction_list, tags=['instruction'])
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        recipe_id = self.kwargs.get('recipe_id')
        user = self.request.user
        get_object_or_404(Recipe, id=recipe_id, user=user)
        return Instruction.objects.filter(user=user, recipe_instructions__id=recipe_id)


class InstructionCreateApiView(generics.CreateAPIView):
    """Instruction Create"""
    permission_classes = [IsAuthenticatedPermission]
    serializer_class = InstructionCreateSerializer

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.recipe = None

    @extend_schema(summary=_('Instruction Create'), responses=response_instruction_create, tags=['instruction'])
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        user = self.request.user
        validated_data = self.request.data
        recipe_id = validated_data.get('recipe_id', None)
        error_message = {'recipe_id': [_('Required field')]}
        if recipe_id:
            self.recipe = Recipe.objects.filter(id=recipe_id, user=user).first()
            error_message['recipe_id'] = [_('There is no recipe with this id')]
            if self.recipe:
                return super().create(request, *args, **kwargs)
        raise ValidationError(error_message)

    def perform_create(self, serializer):
        data = serializer.validated_data
        data['user'] = self.request.user
        instruction = serializer.save()
        self.recipe.instructions.add(instruction)


class InstructionUpdateApiView(generics.UpdateAPIView):
    """Instruction Update"""
    permission_classes = [IsAuthenticatedPermission]
    http_method_names = ['patch']
    serializer_class = InstructionUpdateSerializer

    @extend_schema(summary=_('Instruction Update'), responses=response_instruction_update, tags=['instruction'])
    def patch(self, request, *args, **kwargs):
        return super().patch(request, *args, **kwargs)

    def get_queryset(self):
        user = self.request.user
        return Instruction.objects.filter(user=user)


class InstructionDeleteApiView(generics.DestroyAPIView):
    """Instruction Delete"""
    permission_classes = [IsAuthenticatedPermission]

    @extend_schema(summary=_('Instruction Delete'), responses=response_instruction_delete, tags=['instruction'])
    def delete(self, request, *args, **kwargs):
        return super().delete(request, *args, **kwargs)

    def get_queryset(self):
        user = self.request.user
        return Instruction.objects.filter(user=user)
