from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from config.settings import FRONT_URL


class BaseModelAdmin(admin.ModelAdmin):
    """Base Model Admin"""
    list_per_page = 25


admin.site.site_header = _('NJ List')
admin.site.site_url = FRONT_URL
