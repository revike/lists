from django.utils.translation import gettext_lazy as _
from rest_framework import views, exceptions
from rest_framework.exceptions import APIException
from rest_framework.response import Response


def custom_exception_handler(exc: APIException, context: dict) -> Response:
    """Custom exception handler"""
    response = views.exception_handler(exc, context)
    if isinstance(exc, exceptions.Throttled):
        custom_response_data = {
            'detail': _(f'Try again later.'),
            'seconds': exc.wait if isinstance(exc.wait, int) else None
        }
        if isinstance(exc.wait, int):
            second_text = 'second' if exc.wait == 1 else 'seconds'
            custom_response_data['detail'] = _(f'Try again later. Will be available in {exc.wait} {second_text}.')
        response.data = custom_response_data
    if isinstance(exc, exceptions.ValidationError):
        custom_response_data = {'detail': []}
        codes = exc.get_codes()
        for code in codes:
            detail = exc.detail.get(code, None)
            if isinstance(detail, list) and detail:
                custom_response_data['detail'].append({code: _(f'{detail[0]}')})
            elif isinstance(detail, dict) and detail:
                custom_response_data['detail'].append({code: _(f'{detail}')})
            response.data = custom_response_data
    return response
