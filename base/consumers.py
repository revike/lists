from typing import Coroutine

from channels.generic.websocket import AsyncJsonWebsocketConsumer
from rest_framework import serializers

from users.models import User


class BaseWebsocketConsumer(AsyncJsonWebsocketConsumer):
    """Base Async Json Websocket Consumer"""
    serializer_class: serializers.Serializer = None

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.user: User | None = None

    async def connect(self) -> bool | Coroutine:
        """Connect"""
        await super().connect()
        self.user = self.scope.get('user')
        if not await self.has_permission():
            return await self.close()
        await self.channel_layer.group_add(self.group_name, self.channel_name)
        return True

    @property
    def group_name(self) -> str:
        """Getting the group name"""
        return f'base_group_{self.user.id}'

    @property
    def get_context(self) -> dict:
        """Get context"""

        class AbsoluteUri:
            """Absolut URI"""

            def __call__(self, *args, **kwargs):
                return 'absolut uri'

        class Context:
            """Context"""
            user = self.user
            build_absolute_uri = AbsoluteUri()

        return {'request': Context}

    async def has_permission(self) -> bool:
        """Access only to authorized users"""
        if self.user:
            if self.user.is_active and self.user.is_verified:
                return True
        return False

    async def receive_json(self, content: dict, **kwargs) -> None:
        """Answer to message"""
        await self.send_json({})

    async def disconnect(self, code) -> Coroutine:
        """Disconnect"""
        return super().disconnect(code)

    async def send_json(self, content, close=False) -> Coroutine:
        """Send Json"""
        try:
            return await super().send_json(content, close)
        except RuntimeError:
            await self.disconnect(code=1002)
