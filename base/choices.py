from django.db import models
from django.utils.translation import gettext_lazy as _


class UnitChoice(models.TextChoices):
    """Unit Choices"""
    PCS = 'pcs', _('pcs')
    GRAM = 'gr', _('gr')
    ML = 'ml', _('ml')
    OUNCE = 'ounce', _('ounce')
    POUND = 'pound', _('pound')
    GALLON = 'gallon', _('gallon')
