import base64
import os
import re
import time
from io import BytesIO

from PIL import Image
from rest_framework.generics import QuerySet

from config.settings import MEDIA_URL
from users.models import User


def get_unique_name() -> int:
    """Getting a unique name"""
    return int(time.time() * 10 ** 4)


def save_image(image: str, image_format: str, user: User, db_column: str) -> str:
    """Saving an image"""
    base64_data = re.sub('^data:image/.*;base64,', '', image)
    byte_data = base64.b64decode(base64_data)
    image_data = BytesIO(byte_data)
    img = Image.open(image_data)
    if img.mode in ('RGBA', 'P'):
        img = img.convert('RGB')
    img_name = f'{get_unique_name()}.{image_format}'
    os.makedirs(os.path.join(MEDIA_URL, f'{db_column}/{user.id}'), exist_ok=True)
    result_image_path = f'{db_column}/{user.id}/{img_name}'
    image_path = f'{MEDIA_URL}{result_image_path}'
    img.save(image_path)
    return result_image_path


def get_queryset_users(obj: 'from product.models import ProductList') -> QuerySet[User]:
    """Get queryset users"""
    user_obj = obj.user
    user = User.objects.filter(id=user_obj.id)
    users = obj.users.all().exclude(id=user_obj.id)
    users |= user
    return users.distinct()
