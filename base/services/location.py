import requests
from rest_framework.request import Request


def get_ip_address(request: Request) -> str:
    """Get ip address"""
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    ip_address = request.META.get('REMOTE_ADDR')
    if x_forwarded_for:
        ip_address = x_forwarded_for.split(',')[0]
    return ip_address


def get_location_data(ip: str, language: str = 'en') -> dict:
    """Get location data"""
    request = requests.get(f'http://ip-api.com/json/{ip}?lang={language}')
    if request.status_code == 200:
        result = request.json()
        return result
    return {}
