import json

from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.core.serializers.json import DjangoJSONEncoder
from rest_framework.generics import QuerySet

from config.settings import ENV_TYPE
from users.models import User


def send_websocket_product_lists(users: QuerySet[User]) -> None:
    """Send Changes to Product lists"""
    if ENV_TYPE in ('dev', 'prod',):
        for user in users:
            async_to_sync(get_channel_layer().group_send)(f'group_{user.id}', {
                'type': 'send_product_lists',
                'value': json.dumps({}, cls=DjangoJSONEncoder)
            })


def send_websocket_product_list(users: QuerySet[User], list_id: float | None = None, sleep: float = .1) -> None:
    """Send Changes to Product lists"""
    if ENV_TYPE in ('dev', 'prod',):
        for user in users:
            active_list_id = user.active_list.id if user.active_list else None
            if list_id and list_id == active_list_id:
                data = {'product_list_id': list_id, 'sleep': sleep}
                async_to_sync(get_channel_layer().group_send)(f'group_product_{user.id}', {
                    'type': 'send_update_products',
                    'value': json.dumps(data, cls=DjangoJSONEncoder)
                })


def send_websocket_invitation(recipient: User, sleep: float = .1) -> None:
    """Send Invitations"""
    if ENV_TYPE in ('dev', 'prod',):
        data = {'user_id': recipient.id, 'sleep': sleep}
        async_to_sync(get_channel_layer().group_send)(f'group_invitation_{recipient.id}', {
            'type': 'send_invitations',
            'value': json.dumps(data, cls=DjangoJSONEncoder)
        })


def send_websocket_notification(user: User, sleep: float = .1) -> None:
    """Send websocket notification"""
    if ENV_TYPE in ('dev', 'prod',):
        data = {'user_id': user.id, 'sleep': sleep}
        async_to_sync(get_channel_layer().group_send)(f'group_notification_{user.id}', {
            'type': 'send_notifications',
            'value': json.dumps(data, cls=DjangoJSONEncoder)
        })
