from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.core.mail import send_mail
from config.settings import EMAIL_HOST_USER


class EmailNotification:
    """Email notification"""

    def __init__(self, subject: str, message: str, recipient_list: list, context: dict = None, path_email: str = None,
                 bcc: list = None, headers: dict = None, cc: dict = None, reply_to: dict = None) -> None:
        self.subject = subject
        self.message = message
        self.recipient_list = recipient_list
        self.context = context
        self.path_email = path_email
        self.bcc = bcc
        self.headers = headers
        self.cc = cc
        self.reply_to = reply_to

    def send_email(self):
        """Send email"""
        if self.path_email:
            body = render_to_string(self.path_email, self.context)
            mail = EmailMessage(
                subject=self.subject, body=body, from_email=EMAIL_HOST_USER, to=self.recipient_list, bcc=self.bcc,
                headers=self.headers, cc=self.cc, reply_to=self.reply_to)
            mail.content_subtype = 'html'
            return mail.send()
        return send_mail(self.subject, self.message, EMAIL_HOST_USER, self.recipient_list)
