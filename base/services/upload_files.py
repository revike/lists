def upload_to_icon(instance: 'from product.models import Category', filename: str = '') -> str:
    """Icon storage path"""
    return ''.join([f'icon/{instance.user.id}/', filename])


def upload_to_avatar(instance: 'from users.models import UserProfile', filename: str = '') -> str:
    """Avatar storage path"""
    return ''.join([f'avatar/{instance.user.id}/', filename])


def upload_to_image_recipe(instance: 'from recipe.models import Recipe', filename: str = '') -> str:
    """Image storage path"""
    return ''.join([f'image_recipe/{instance.user.id}/', filename])
