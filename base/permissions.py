from rest_framework.permissions import IsAuthenticated


class IsAuthenticatedPermission(IsAuthenticated):
    """Authenticated Permission"""

    def has_permission(self, request, view):
        if bool(request.user and request.user.is_authenticated):
            return bool(request.user.is_verified and request.user.is_active)
