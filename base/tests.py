import datetime

import pytz
import json
from typing import Any

from django.contrib.auth.hashers import make_password
from rest_framework.test import APITestCase, APIClient
from rest_framework import status, serializers
from django.urls import reverse
from config.settings import TIME_ZONE, DATETIME_FORMAT_DRF
from product.models import Category, ProductList, Product
from users.models import User


class BaseTestCase(APITestCase):
    """Base class for testing the entire project"""
    access_token: str
    password: str = '123qwe456rty!S'
    password_hash = make_password(password)

    def setUp(self) -> None:
        self.maxDiff = None
        self.new_user = {
            'username': 'username',
            'email': 'user@example.com',
            'is_active': True,
            'is_verified': True
        }
        self.user = User.objects.create(**self.new_user)
        self.user.password = make_password(self.password)
        self.user.save(update_fields=['password'])

        self.new_user2 = {
            'username': 'username2',
            'email': 'user2@example.com',
            'is_active': True,
            'is_verified': True
        }
        self.user2 = User.objects.create(**self.new_user2)
        self.user2.password = make_password(self.password)
        self.user2.save(update_fields=['password'])

        self.category_data = {'title': 'my category'}
        self.category_user = Category.objects.create(user=self.user, **self.category_data)
        self.category_user2 = Category.objects.create(user=self.user2, **self.category_data)

        self.pl = ProductList.objects.create(title='list user', user=self.user)
        self.pl.users.add(self.user2)

        self.product_data = {
            'user': self.user,
            'product_list': self.pl,
            'title': 'title'
        }
        self.product = Product.objects.create(**self.product_data)

        self.pl_user2 = ProductList.objects.create(title='list user2', user=self.user2)
        self.pl.users.add(self.user)
        self.product_data_user2 = {
            'user': self.user2,
            'product_list': self.pl_user2,
            'title': 'title'
        }
        self.product_user2 = Product.objects.create(**self.product_data)

    @staticmethod
    def get_serialized_data(user, serializer_class: serializers) -> str:
        """Serialized data return method"""
        return serializer_class(user).data

    @staticmethod
    def get_datetime_with_tz_as_string(datetime_item: datetime.datetime) -> str:
        return datetime_item.astimezone(pytz.timezone(TIME_ZONE)).strftime(DATETIME_FORMAT_DRF)

    def _login(self, email: str) -> None:
        self.client = APIClient()
        response = self.client.post(reverse('user:login'), {'email': email, 'password': self.password})
        self.access_token = response.json().get('access')
        if self.access_token is None:
            access_token = response.cookies.get('access')
            self.access_token = access_token._value

    def _make_request(self, method: str, url: str, user_email: str | None = None, data: object = None,
                      status_code: int = status.HTTP_200_OK, headers: dict = None) -> dict | None:
        """Method for generating a request with authorization and TODO: closed access testing"""
        headers_to_send = {}
        if user_email:
            self._login(user_email)
            headers_to_send = {"HTTP_AUTHORIZATION": f'Bearer {self.access_token}'}
            if headers is not None:
                headers_to_send.update(headers)
        request_data = {
            'path': url,
            'content_type': 'application/json',
        }
        request_data.update(headers_to_send)

        if data and method.lower() != 'get':
            request_data['data'] = json.dumps(data)
        elif method.lower() == 'get':
            request_data['data'] = data

        response = None

        if method == 'POST':
            response = self.client.post(**request_data)
        elif method == 'GET':
            response = self.client.get(**request_data)
        elif method == 'PATCH':
            response = self.client.patch(**request_data)
        elif method == 'PUT':
            response = self.client.put(**request_data)
        elif method == 'DELETE':
            response = self.client.delete(**request_data)

        self.assertIsNotNone(response)

        self.assertEqual(response.status_code, status_code)
        if method != 'DELETE':
            return response.json()

    def make_post(self, url: str, user_email: str | None, data: Any, status_code: int, headers: dict = None) -> dict:
        return self._make_request('POST', url, user_email, data, status_code, headers)

    def make_get(self, url: str, user_email: str | None = None, params: dict = None,
                 status_code: int = status.HTTP_200_OK, headers: dict = None) -> dict:
        return self._make_request('GET', url, user_email, params, status_code, headers)

    def make_patch(self, url: str, user_email: str | None, data: dict, status_code: int = status.HTTP_200_OK,
                   headers: dict = None) -> dict:
        return self._make_request('PATCH', url, user_email, data, status_code, headers)

    def make_put(self, url: str, user_email: str | None, data: dict, status_code: int = status.HTTP_200_OK,
                 headers: dict = None) -> dict:
        return self._make_request('PUT', url, user_email, data, status_code, headers)

    def make_delete(self, url: str, user_email: str | None, data: dict = None,
                    status_code: int = status.HTTP_204_NO_CONTENT, headers: dict = None) -> None:
        return self._make_request('DELETE', url, user_email, data, status_code, headers)
