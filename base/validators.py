import datetime
import re

import phonenumbers
import pytz
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from config.settings import TIME_ZONE, MIN_AGE_USER


def email_validate(email: str) -> re.Match | None | bool:
    """Email validate"""
    if email:
        return re.fullmatch(r"[^@]+@[^@]+\.[^@]+", f'{email}')
    return False


def validate_phone(value: str) -> str:
    """Validate for phone"""
    error = _('Invalid phone number format')
    try:
        pattern = r"^\+\d{8,15}$"
        number = phonenumbers.parse(value)
        if phonenumbers.is_valid_number(number) and re.search(pattern, value):
            return value
        raise ValidationError(error, params={'value': value})
    except phonenumbers.NumberParseException:
        raise ValidationError(error, params={'value': value})


def validate_birthday(value: datetime.date) -> datetime.date:
    """Validate birthday"""
    error = _(f'Invalid birthday. Min age {MIN_AGE_USER}')
    datetime_now = datetime.datetime.now().astimezone(pytz.timezone(TIME_ZONE))
    age = datetime_now.year - value.year - ((datetime_now.month, datetime_now.day) < (value.month, value.day))
    if age < MIN_AGE_USER:
        raise ValidationError(error)
    return value
