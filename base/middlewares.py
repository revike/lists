import datetime
import functools
from typing import Coroutine

import pytz
from channels.db import database_sync_to_async
from channels.middleware import BaseMiddleware
from django.contrib.auth.models import AnonymousUser
from django.urls import reverse, resolve
from django.utils import timezone
from django.db import connection
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework_simplejwt.exceptions import TokenError
from rest_framework_simplejwt.tokens import AccessToken

from base.services.location import get_ip_address, get_location_data
from config.settings import SECURE_HTTP, SAME_SITE, COOKIE_DOMAIN, DEBUG, ENV_TYPE
from users.models import User


class AuthWebSocketMiddleware(BaseMiddleware):
    """Auth WebSocket Middleware"""

    async def __call__(self, scope, receive, send) -> Coroutine:
        query_string = scope.get('query_string', None)
        token = None
        if query_string:
            token = query_string.decode().split('=')[-1]
        scope['user'] = await self.get_user(token)
        return await super().__call__(scope, receive, send)

    @database_sync_to_async
    def get_user(self, token: AccessToken) -> Coroutine:
        user = AnonymousUser()
        if token:
            try:
                access_token = AccessToken(token)
                user_id = access_token.payload.get('user_id')
                user = User.objects.get(id=user_id, is_active=True, is_verified=True)
            except (TokenError, User.DoesNotExist):
                ...
        return user


class TimezoneMiddleware:
    """Timezone Middleware"""

    def __init__(self, get_response) -> None:
        self.get_response = get_response

    def __call__(self, request: Request) -> Response:
        timezone.deactivate()
        all_tz = pytz.all_timezones
        url_request, url, time_zone, read_to_cookie = request.path, reverse('admin:index'), None, False
        if url in url_request:
            cookies = request.COOKIES
            time_zone = cookies.get('timezone')
            if time_zone not in all_tz:
                user = request.user
                if user != AnonymousUser():
                    ip_address = get_ip_address(request)
                    location = get_location_data(ip_address)
                    time_zone, read_to_cookie = location.get('timezone'), True
        if time_zone in all_tz:
            timezone.activate(pytz.timezone(time_zone))
        response = self.get_response(request)
        if read_to_cookie:
            response.set_cookie(key='timezone', value=time_zone, max_age=datetime.timedelta(hours=1), httponly=False,
                                secure=SECURE_HTTP, samesite=SAME_SITE, domain=COOKIE_DOMAIN)
        return response


class DebuggerMiddleware:
    """Debugger Middleware"""

    def __init__(self, get_response) -> None:
        self.get_response = get_response

    @staticmethod
    def query_debugger(func) -> object:
        """Query debugger"""

        @functools.wraps(func)
        def inner_func(*args, **kwargs):
            """Inner func"""
            start_queries = len(connection.queries)
            result = func(*args, **kwargs)
            end_queries = len(connection.queries)
            url = args[-1].path
            view_func = resolve(url).func
            if getattr(view_func, 'view_class', None):
                view_func = resolve(url).func.view_class
            print(f'URL : {url}')
            print(f'View : {view_func}')
            print(f'Number of Queries : {end_queries - start_queries}')
            return result

        if DEBUG and ENV_TYPE in ('local',):
            return inner_func
        return func

    @query_debugger
    def __call__(self, request: Request):
        return self.get_response(request)
