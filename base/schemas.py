from rest_framework import status

from base.serializers import BaseManyDummySerializer, BaseDummySerializer, ThrottleDummySerializer

base_responses = {
    status.HTTP_400_BAD_REQUEST: BaseManyDummySerializer,
    status.HTTP_401_UNAUTHORIZED: BaseDummySerializer,
    status.HTTP_404_NOT_FOUND: BaseDummySerializer,
    status.HTTP_405_METHOD_NOT_ALLOWED: BaseDummySerializer,
    status.HTTP_429_TOO_MANY_REQUESTS: ThrottleDummySerializer
}
