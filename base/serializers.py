from rest_framework import serializers
from django.utils.translation import gettext_lazy as _

from base.services.utils import save_image


class Base64FileField(serializers.FileField):
    """Basic file save serializer"""

    def to_internal_value(self, data):
        user = None
        request = self.context.get('request')
        if request is not None:
            user = request.user
        image = data.startswith('data:image')
        column = self.source
        if isinstance(data, str) and image:
            file_format, file_str = data.split(';base64,')
            ext = file_format.split('/')[-1]
            if image:
                file = save_image(file_str, ext, user, column)
                return file


class BaseDummySerializer(serializers.Serializer):
    """Base Dummy Serializer"""
    detail = serializers.StringRelatedField(default=_('status message'))


class BaseKeyDummySerializer(serializers.Serializer):
    """Base Key Dummy Serializer"""
    key = serializers.StringRelatedField()


class BaseManyDummySerializer(serializers.Serializer):
    """Base Many Dummy Serializer"""
    detail = BaseKeyDummySerializer(many=True)


class ThrottleDummySerializer(BaseDummySerializer):
    """Throttle Dummy Serializer"""
    seconds = serializers.IntegerField(default=60)
