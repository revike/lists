from rest_framework import serializers

from notice.choices import NotificationType
from notice.models import Notice, Notification
from notice.utils import get_other_product_list
from product.serializers import ListDetailForProductSerializer
from users.serializers import UserForListSerializer


class ResponseToInvitationSerializer(serializers.ModelSerializer):
    """Response to Invitation Serializer"""
    is_reaction = serializers.BooleanField(read_only=True)
    sender = UserForListSerializer(read_only=True)
    recipient = UserForListSerializer(read_only=True)
    product_list = ListDetailForProductSerializer(read_only=True)
    is_agree = serializers.BooleanField()

    class Meta:
        model = Notice
        fields = ['id', 'sender', 'recipient', 'product_list', 'is_reaction', 'is_agree', 'created', 'updated']

    def update(self, instance, validated_data):
        notification_type = NotificationType.NotInvitation
        product_list = instance.product_list
        recipient = instance.recipient
        other = get_other_product_list(product_list, recipient)
        instance.is_reaction, instance.is_agree = True, False
        is_agree = validated_data.get('is_agree', None)
        if is_agree:
            instance.is_agree = True
            product_list.users.add(recipient)
            notification_type = NotificationType.Invitation
        Notification.objects.create(user=instance.sender, notification_type=notification_type, other=other)
        return super().update(instance, validated_data)


class NotificationReadSerializer(serializers.ModelSerializer):
    """Notification Read Serializer"""
    message = serializers.CharField(read_only=True)
    other = serializers.DictField(read_only=True, help_text={"detail": {"key": "value"}})
    notification_type = serializers.ChoiceField(choices=NotificationType.choices, read_only=True)
    user = UserForListSerializer(read_only=True)

    class Meta:
        model = Notification
        fields = '__all__'


class NoticeListRemoveUserSerializer(serializers.Serializer):
    """Product Notice List remove User serializer"""
    user_id = serializers.IntegerField()
