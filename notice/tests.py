import random

from django.urls import resolve
from rest_framework import status
from rest_framework.reverse import reverse

from base.tests import BaseTestCase
from notice import views
from notice.choices import NotificationType
from notice.models import Notice, Notification


class TestNoticeCase(BaseTestCase):
    """Test Notice Case"""

    def setUp(self) -> None:
        super().setUp()
        self.notice = Notice.objects.create(sender=self.user, recipient=self.user2, product_list=self.pl)
        self.notice_user2 = Notice.objects.create(sender=self.user2, recipient=self.user, product_list=self.pl_user2)
        self.notification = Notification.objects.create(user=self.user, notification_type=NotificationType.Register)

        for i in range(5):
            notification_type = random.choice(NotificationType.values)
            Notification.objects.create(user=self.user, notification_type=notification_type)

    def test_response_invitation(self):
        """Test Response Invitation"""
        url = reverse('notice:response_invitation', kwargs={'pk': self.notice.id})
        view = views.ResponseToInvitationAPIView
        self.pl.users.remove(self.user2)
        pl_users_start = self.pl.users.all()
        self.make_patch(url, None, {} or {'is_agree': False}, status_code=status.HTTP_401_UNAUTHORIZED)
        self.make_patch(url, self.user.email, {'is_agree': False}, status_code=status.HTTP_404_NOT_FOUND)
        self.make_patch(url, self.user2.email, {'is_agree': False}, status_code=status.HTTP_200_OK)
        self.make_patch(url, self.user2.email, {'is_agree': False}, status_code=status.HTTP_404_NOT_FOUND)
        pl_users_finish = self.pl.users.all()
        self.assertEqual(len(pl_users_start), len(pl_users_finish))
        self.notice.is_reaction = False
        self.notice.save(update_fields=['is_reaction'])
        self.make_patch(url, self.user2.email, {'is_agree': True}, status_code=status.HTTP_200_OK)
        pl_users_finish2 = self.pl.users.all()
        self.assertEqual(len(pl_users_start) + 1, len(pl_users_finish2))
        self.assertEqual(resolve(url).func.view_class, view)

    def test_response_invitations_all(self):
        """Test response invitations all"""
        notice_reaction, notice_is_agree = self.notice_user2.is_reaction, self.notice_user2.is_agree
        url, view = reverse('notice:response_invitation_all'), views.ResponseToInvitationAllAPIView
        self.make_post(url, None, {}, status_code=status.HTTP_401_UNAUTHORIZED)
        self.make_post(url, self.user.email, {}, status_code=status.HTTP_200_OK)
        notice_finish = Notice.objects.get(id=self.notice_user2.id)
        notice_reaction_finish, notice_is_agree_finish = notice_finish.is_reaction, notice_finish.is_agree
        self.assertNotEqual(notice_reaction, notice_reaction_finish)
        self.assertNotEqual(notice_is_agree, notice_is_agree_finish)
        self.assertEqual(resolve(url).func.view_class, view)

    def test_notification_read(self):
        """Test Notification read"""
        read = self.notification.is_read
        url = reverse('notice:notification_read', kwargs={'pk': self.notification.id})
        view = views.NotificationReadAPIView
        self.make_patch(url, None, data={'is_read': True}, status_code=status.HTTP_401_UNAUTHORIZED)
        response = self.make_patch(url, self.user.email, data={'is_read': True}, status_code=status.HTTP_200_OK)
        self.assertEqual(response.get('id'), self.notification.id)
        self.assertNotEqual(response.get('is_read'), self.notification.is_read)
        notification = Notification.objects.get(id=self.notification.id)
        self.assertNotEqual(read, notification.is_read)
        self.assertEqual(resolve(url).func.view_class, view)

    def test_notification_read_all(self):
        """Test Notification read all"""
        notifications_not_read_count = Notification.objects.filter(is_read=False, user=self.user).count()
        url, view = reverse('notice:notifications_read_all'), views.NotificationReadAllAPIView
        self.make_post(url, None, {}, status_code=status.HTTP_401_UNAUTHORIZED)
        response = self.make_post(url, self.user.email, {}, status_code=status.HTTP_200_OK)
        self.assertEqual(response.get('detail'), 'ok')
        notifications_not_read_count_end = Notification.objects.filter(is_read=False, user=self.user).count()
        self.assertNotEqual(notifications_not_read_count, notifications_not_read_count_end)
        self.assertEqual(resolve(url).func.view_class, view)

    def test_notice_list_remove_user(self):
        """Test notice list remove user"""
        notices = Notice.objects.all()
        data = {'user_id': self.user2.id}
        url = reverse('notice:notice_list_remove_user', kwargs={'pk': self.pl.id})
        view = views.NoticeListRemoveUserAPIView
        self.make_patch(url, None, data, status_code=status.HTTP_401_UNAUTHORIZED)
        response = self.make_patch(url, self.user.email, data)
        if notices.count():
            self.assertEqual(notices.count() - 1, len(response))
        self.make_patch(url, self.user.email, data, status_code=status.HTTP_400_BAD_REQUEST)
        self.assertEqual(resolve(url).func.view_class, view)
