from product.models import ProductList
from users.models import User


def get_other_product_list(instance: ProductList, user: User) -> dict:
    """Get other product list"""
    return {
        'product_list': {
            'id': instance.id,
            'title': instance.title,
        },
        'user': {
            'id': user.id,
            'email': user.email,
            'username': user.username,
            'first_name': user.first_name,
            'last_name': user.last_name,
        }
    }
