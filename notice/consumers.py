import asyncio
import json
from typing import Dict, List, Any

from asgiref.sync import sync_to_async

from base.consumers import BaseWebsocketConsumer
from notice.models import Notice, Notification
from notice.serializers import ResponseToInvitationSerializer, NotificationReadSerializer


class InvitationConsumer(BaseWebsocketConsumer):
    """Invitation Consumer"""
    serializer_class = ResponseToInvitationSerializer

    async def connect(self) -> None:
        """Connect"""
        connect = await super().connect()
        if connect:
            await self.send_json(await self.get_data(self.user.id))

    @property
    def group_name(self) -> str:
        """Getting the group name"""
        return f'group_invitation_{self.user.id}'

    @sync_to_async
    def get_notices(self, user_id: int) -> List[Dict | Any]:
        """Get notices"""
        result = []
        notices = Notice.objects.filter(recipient_id=user_id, is_reaction=False).select_related(
            'product_list', 'sender', 'recipient', 'sender__profile', 'recipient__profile')
        if notices:
            notices_serializer = self.serializer_class(notices, many=True, context=self.get_context)
            result = notices_serializer.data
        return result

    async def get_data(self, user_id: int) -> List[Dict | Any]:
        """Get data"""
        data = await self.get_notices(user_id)
        return data

    async def send_invitations(self, event: dict) -> None:
        data = json.loads(event.get('value'))
        if data:
            sleep = data.get('sleep', .2)
            await asyncio.sleep(sleep)
            user_id = data.get('user_id', None)
            notice_data = await self.get_data(user_id)
            await self.send_json(notice_data)

    async def receive_json(self, content: dict, **kwargs) -> None:
        """Answer to message"""
        await self.send_json(await self.get_data(user_id=self.user.id))


class NotificationConsumer(BaseWebsocketConsumer):
    """Notification Consumer"""
    serializer_class = NotificationReadSerializer

    async def connect(self) -> None:
        """Connect"""
        connect = await super().connect()
        if connect:
            await self.send_json(await self.get_data(self.user.id))

    @property
    def group_name(self) -> str:
        """Getting the group name"""
        return f'group_notification_{self.user.id}'

    async def get_data(self, user_id: int) -> List[Dict | Any]:
        """Get data"""
        data = await self.get_notifications(user_id)
        return data

    @sync_to_async
    def get_notifications(self, user_id: int) -> List[Dict | Any]:
        """Get notifications"""
        result = []
        notifications = Notification.objects.filter(user_id=user_id).select_related()
        if notifications:
            notification_serializer = self.serializer_class(notifications, many=True, context=self.get_context)
            result = notification_serializer.data
        return result

    async def send_notifications(self, event: dict) -> None:
        data = json.loads(event.get('value'))
        if data:
            sleep = data.get('sleep', .2)
            await asyncio.sleep(sleep)
            user_id = data.get('user_id', None)
            data = await self.get_data(user_id)
            await self.send_json(data)

    async def receive_json(self, content: dict, **kwargs) -> None:
        """Answer to message"""
        await self.send_json(await self.get_data(self.user.id))
