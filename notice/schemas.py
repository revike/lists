from rest_framework import status

from base.schemas import base_responses
from base.serializers import BaseDummySerializer
from notice.serializers import ResponseToInvitationSerializer, NotificationReadSerializer
from users.serializers import UserForListSerializer

responses_to_invitation = base_responses.copy()
responses_to_invitation[status.HTTP_200_OK] = ResponseToInvitationSerializer

responses_to_invitation_all = base_responses.copy()
del responses_to_invitation_all[status.HTTP_404_NOT_FOUND]
responses_to_invitation_all[status.HTTP_200_OK] = BaseDummySerializer

responses_notification_read = base_responses.copy()
responses_notification_read[status.HTTP_200_OK] = NotificationReadSerializer

responses_notification_read_all = base_responses.copy()
del responses_notification_read_all[status.HTTP_404_NOT_FOUND]
responses_notification_read_all[status.HTTP_200_OK] = BaseDummySerializer

responses_notice_remove_user = base_responses.copy()
del responses_notice_remove_user[status.HTTP_404_NOT_FOUND]
responses_notice_remove_user[status.HTTP_200_OK] = UserForListSerializer(many=True)
