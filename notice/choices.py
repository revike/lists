from django.db import models
from django.utils.translation import gettext_lazy as _


class NotificationType(models.TextChoices):
    """Notification Type Choices"""
    Register = 'register', _('register')
    Invitation = 'invitation', _('invitation')
    NotInvitation = 'not_invitation', _('not_invitation')
    CameOutList = 'came_out_list', _('came_out_list')
