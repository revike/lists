from django.urls import path

from notice.views import ResponseToInvitationAPIView, NotificationReadAPIView, NotificationReadAllAPIView, \
    NoticeListRemoveUserAPIView, ResponseToInvitationAllAPIView

app_name = 'notice'

urlpatterns = [
    path('response/<int:pk>/', ResponseToInvitationAPIView.as_view(), name='response_invitation'),
    path('response/all/', ResponseToInvitationAllAPIView.as_view(), name='response_invitation_all'),
    path('notification/read/<int:pk>/', NotificationReadAPIView.as_view(), name='notification_read'),
    path('notification/read/all/', NotificationReadAllAPIView.as_view(), name='notifications_read_all'),

    path('list/remove-user/<int:pk>/', NoticeListRemoveUserAPIView.as_view(), name='notice_list_remove_user'),
]
