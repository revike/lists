from django.contrib import admin

from notice.models import Notice, Notification


@admin.register(Notice)
class NoticeAdmin(admin.ModelAdmin):
    """Notice Admin Panel"""
    list_display = ['id', 'product_list', 'sender', 'recipient', 'is_reaction', 'created', 'updated']
    list_display_links = list_display
    fields = ['id', 'sender', 'recipient', 'product_list', 'is_reaction', 'is_agree', 'created', 'updated']
    readonly_fields = ['id', 'created', 'updated']
    list_filter = ['is_reaction', 'is_agree']
    search_fields = ['product_list__title', 'sender__email', 'recipient__email']

    def has_change_permission(self, request, obj=None):
        pass

    def has_add_permission(self, request):
        pass


@admin.register(Notification)
class NotificationAdmin(admin.ModelAdmin):
    """Notification Admin Panel"""
    list_display = ['id', 'user', 'notification_type', 'is_read', 'created', 'updated']
    list_display_links = list_display
    fields = ['id', 'user', 'notification_type', 'message', 'other', 'is_read', 'created', 'updated']
    readonly_fields = fields
    list_filter = ['is_read', 'notification_type']
    search_fields = ['user__email', 'user__username']
    
    def get_readonly_fields(self, request, obj=None):
        if not obj:
            return ['id', 'created', 'updated']
        return super().get_readonly_fields(request, obj)
