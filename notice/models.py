from django.db import models
from django.utils.translation import gettext_lazy as _

from base.models import BaseModel, NULLABLE
from base.services.websocket_messages import send_websocket_notification
from notice.choices import NotificationType
from product.models import ProductList
from users.models import User


class Notice(BaseModel):
    """Notice"""
    sender = models.ForeignKey(User, on_delete=models.CASCADE, related_name='sender_notices', verbose_name=_('sender'))
    recipient = models.ForeignKey(User, on_delete=models.CASCADE,
                                  related_name='recipient_notices', verbose_name=_('recipient'))
    product_list = models.ForeignKey(ProductList, on_delete=models.CASCADE,
                                     related_name='notices', verbose_name=_('product list'))
    is_reaction = models.BooleanField(_('is reaction'), default=False)
    is_agree = models.BooleanField(_('is agree'), default=False)

    class Meta:
        verbose_name = _('notice')
        verbose_name_plural = _('notices')
        ordering = ['-updated']

    def __str__(self):
        return f'{self.sender} -> {self.recipient}'


class Notification(BaseModel):
    """Notification"""
    notification_type = models.CharField(_('type'), max_length=16, choices=NotificationType.choices)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_notification', verbose_name=_('user'))
    message = models.TextField(_('message'), **NULLABLE, max_length=128)
    other = models.JSONField(_('other'), **NULLABLE)
    is_read = models.BooleanField(_('is read'), default=False)

    class Meta:
        verbose_name = _('notification')
        verbose_name_plural = _('notifications')
        ordering = ['-created']

    def __str__(self):
        return f'{self.user}'

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        send_websocket_notification(self.user)

    def delete(self, using=None, keep_parents=False):
        send_websocket_notification(self.user, sleep=2)
        return super().delete(using, keep_parents)
