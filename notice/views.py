from django.utils.translation import gettext_lazy as _
from drf_spectacular.utils import extend_schema
from rest_framework import generics, status
from rest_framework.response import Response

from base.permissions import IsAuthenticatedPermission
from base.services.utils import get_queryset_users
from base.services.websocket_messages import send_websocket_invitation, send_websocket_product_list, \
    send_websocket_notification
from notice.choices import NotificationType
from notice.models import Notice, Notification
from notice.schemas import responses_to_invitation, responses_notification_read, responses_notification_read_all, \
    responses_notice_remove_user, responses_to_invitation_all
from notice.serializers import ResponseToInvitationSerializer, NotificationReadSerializer, \
    NoticeListRemoveUserSerializer
from notice.utils import get_other_product_list
from product.models import ProductList
from users.serializers import UserForListSerializer


class ResponseToInvitationAPIView(generics.UpdateAPIView):
    """Response to Invitation"""
    permission_classes = [IsAuthenticatedPermission]
    serializer_class = ResponseToInvitationSerializer
    http_method_names = ['patch']

    @extend_schema(summary=_('Notice'), responses=responses_to_invitation, tags=['notice'])
    def patch(self, request, *args, **kwargs):
        return super().patch(request, *args, **kwargs)

    def get_queryset(self):
        user = self.request.user
        return Notice.objects.filter(recipient=user, is_reaction=False).select_related(
            'product_list', 'sender', 'recipient', 'sender__profile', 'recipient__profile')

    def perform_update(self, serializer):
        notice = serializer.save()
        product_list = notice.product_list
        if product_list:
            users = get_queryset_users(product_list)
            send_websocket_invitation(self.request.user)
            send_websocket_product_list(users, product_list.id, 2)


class ResponseToInvitationAllAPIView(generics.CreateAPIView):
    """Response to invitation all"""
    permission_classes = [IsAuthenticatedPermission]

    @extend_schema(summary=_('Notice request all'), responses=responses_to_invitation_all, tags=['notice'])
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        data = {'detail': [{'notice': _('Notices not found')}]}
        response_status = status.HTTP_400_BAD_REQUEST
        user = self.request.user
        notices = Notice.objects.filter(recipient=user, is_reaction=False).select_related('sender', 'product_list')
        if notices:
            data, response_status = {'detail': _('ok')}, status.HTTP_200_OK
            product_lists_id = notices.values_list('product_list', flat=True)
            send_ws_products = {}
            create_notifications = []
            for notice in notices:
                notice.is_reaction, notice.is_agree = True, True
                notice.save()
                data_create_notifications = Notification(
                    user=notice.sender,
                    notification_type=NotificationType.Invitation,
                    other=get_other_product_list(notice.product_list, user)
                )
                create_notifications.append(data_create_notifications)
            send_websocket_invitation(user)
            product_lists = ProductList.objects.filter(id__in=product_lists_id).select_related()
            for product_list in product_lists:
                product_list.users.add(user)
                users = get_queryset_users(product_list)
                send_ws_products[product_list.id] = users
            for send_ws_product in send_ws_products:
                send_websocket_product_list(send_ws_products[send_ws_product], send_ws_product)
            Notification.objects.bulk_create(create_notifications)
            [send_websocket_notification(notification.user) for notification in create_notifications]
        return Response(data, status=response_status)


class NotificationReadAPIView(generics.UpdateAPIView):
    """Notification Read"""
    permission_classes = [IsAuthenticatedPermission]
    serializer_class = NotificationReadSerializer
    http_method_names = ['patch']

    @extend_schema(summary=_('Notification read'), responses=responses_notification_read, tags=['notice'])
    def patch(self, request, *args, **kwargs):
        return super().patch(request, *args, **kwargs)

    def get_queryset(self):
        user = self.request.user
        return Notification.objects.filter(user=user).select_related('user', 'user__profile')


class NotificationReadAllAPIView(generics.CreateAPIView):
    """Notifications read all"""
    permission_classes = [IsAuthenticatedPermission]

    @extend_schema(summary=_('Notification read all'), responses=responses_notification_read_all, tags=['notice'])
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        data = {'detail': [{'notification': _('Notifications not found')}]}
        response_status = status.HTTP_400_BAD_REQUEST
        user = self.request.user
        notifications = Notification.objects.filter(user=user, is_read=False)
        if notifications:
            data['detail'] = _('ok')
            response_status = status.HTTP_200_OK
            update = notifications.update(is_read=True)
            if update:
                send_websocket_notification(user)
        return Response(data, response_status)


class NoticeListRemoveUserAPIView(generics.UpdateAPIView):
    """Notice List Remove User"""
    permission_classes = [IsAuthenticatedPermission]
    serializer_class = NoticeListRemoveUserSerializer
    http_method_names = ['patch']

    @extend_schema(summary=_('Notice remove user'), responses=responses_notice_remove_user, tags=['product list'])
    def patch(self, request, *args, **kwargs):
        return super().patch(request, *args, **kwargs)

    def get_object(self):
        user = self.request.user
        product_list_id = self.kwargs.get('pk')
        data = self.request.data
        user_id = data.get('user_id')
        notices = Notice.objects.filter(sender=user, recipient_id=user_id, product_list_id=product_list_id,
                                        product_list__user=user, is_reaction=False).select_related()
        return notices.first()

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        user = self.request.user
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        product_list = instance.product_list if instance else None
        if product_list:
            send_websocket_invitation(instance.recipient, sleep=2)
            instance.delete()
            if product_list.user == user:
                notices = Notice.objects.filter(
                    sender=user, is_reaction=False, product_list=product_list).select_related(
                    'recipient__profile', 'recipient')
                recipients = (notice.recipient for notice in notices)
                recipients_data = UserForListSerializer(recipients, many=True).data
                return Response(recipients_data)
        return Response({'detail': [{'recipient': _('Recipient not found')}]}, status=status.HTTP_400_BAD_REQUEST)
