from channels.routing import URLRouter
from django.urls import path
from base.middlewares import AuthWebSocketMiddleware
from notice.consumers import InvitationConsumer, NotificationConsumer

from product.consumers import ListConsumer, ProductConsumer

websockets = AuthWebSocketMiddleware(
    URLRouter([
        path('ws/lists/', ListConsumer.as_asgi()),
        path('ws/products/', ProductConsumer.as_asgi()),
        path('ws/invitations/', InvitationConsumer.as_asgi()),
        path('ws/notifications/', NotificationConsumer.as_asgi()),
    ])
)
