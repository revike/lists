# NJ List

Основные технологии
-------------------

```
* Python 3.12.0
* Django 5.0.2
* Django REST Framework 3.14.0
* Postgresql 14.5
```

Установка и запуск
------------------

* Переходим в директорию проекта

```shell
cd backend
```

* Создаем файл .env такой же, как .env.example (меняем настройки при необходимости)

```shell
touch .env
```

* Создаем виртуальное окружение

```shell
python3 -m venv venv
```

* Активируем виртуальное окружение или [запускаем с помощью docker-compose](#docker)

```shell
source venv/bin/activate
```

* Устанавливаем зависимости

```shell
pip install -r requirements.txt
```

* Выполняем миграции

```shell
python3 manage.py migrate
```

* Собираем статику

```shell
python3 manage.py collectstatic
```

* Запуск

```shell
python3 manage.py runserver
```

<a name="docker"></a> Запуск с помощью docker-compose
-------------------------------

* Делаем файл docker_commands.sh исполняемым

```shell
chmod +x docker_commands.sh
```

* Запуск

```shell
docker-compose up -d --build
```

Тесты
-----

* Устанавливаем флаг test в ENV_TYPE в файле .env

```dotenv
ENV_TYPE=test
```

```shell
python3 manage.py test
```
