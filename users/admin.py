from django.contrib import admin
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _

from base.admin import BaseModelAdmin
from config.settings import BACK_URL, MEDIA_URL
from users.forms import UserForm
from users.models import User, UserProfile


class InlineProfile(admin.StackedInline):
    """inline Profile"""
    model = UserProfile
    extra = 0
    fields = ['gender', 'birthday', 'phone', 'avatar', 'get_preview_avatar', 'created', 'updated']
    readonly_fields = ['get_preview_avatar', 'created', 'updated']

    def get_preview_avatar(self, obj):
        if obj.icon:
            return mark_safe(f'<img src="{BACK_URL}/{MEDIA_URL}{obj.avatar}" style="max-height: 200px;">')

    get_preview_avatar.short_description = _('avatar preview')


@admin.register(User)
class UserAdmin(BaseModelAdmin):
    """User Admin panel"""
    form = UserForm
    inlines = [InlineProfile]
    list_display = ['id', 'username', 'email', 'first_name', 'last_name', 'created', 'updated']
    list_display_links = list_display
    fields = ['id', 'username', 'email', 'first_name', 'last_name', 'password', 'groups', 'is_active', 'is_superuser',
              'is_staff', 'is_verified', 'activation_key', 'activation_key_expires', 'delay', 'created', 'updated']
    readonly_fields = ['id', 'activation_key', 'activation_key_expires', 'created', 'updated']
    list_filter = ['is_active', 'is_superuser', 'is_staff']
    search_fields = ['email', 'username', 'first_name', 'last_name']
    filter_horizontal = ['groups']
