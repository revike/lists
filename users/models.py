import os

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.dispatch import receiver
from django.utils.translation import gettext_lazy as _

from base.models import LowerCaseEmailField, NULLABLE, BaseModel, LowerCaseUsernameField
from base.services.upload_files import upload_to_avatar
from base.validators import validate_birthday, validate_phone
from config.settings import MEDIA_URL
from users.choices import GenderChoice


class User(AbstractUser, BaseModel):
    """User model"""
    __activation_key = None
    username = LowerCaseUsernameField(_('username'), max_length=150, unique=True)
    email = LowerCaseEmailField(_('email address'), unique=True)
    is_verified = models.BooleanField(_('is verified'), default=True)
    activation_key = models.CharField(_('activation key'), max_length=150, **NULLABLE)
    activation_key_expires = models.DateTimeField(_('activation key expires'), **NULLABLE)
    delay = models.PositiveSmallIntegerField(_('delay'), default=1)
    active_list = models.ForeignKey('product.ProductList', on_delete=models.SET_NULL, **NULLABLE,
                                    related_name='list_users', verbose_name=_('active list'))

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__activation_key = self.activation_key

    def __str__(self):
        return f'{self.email}'

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')


class UserProfile(BaseModel):
    """Profile user"""
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile', verbose_name=_('user'))
    gender = models.CharField(_('gender'), max_length=3, choices=GenderChoice.choices, default=GenderChoice.NA)
    birthday = models.DateField(_('birthday'), validators=[validate_birthday], **NULLABLE)
    phone = models.CharField(_('phone'), max_length=16, validators=[validate_phone], **NULLABLE, unique=True)
    avatar = models.ImageField(_('avatar'), upload_to=upload_to_avatar, **NULLABLE)
    __avatar = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__avatar = self.avatar

    def __str__(self):
        return f'{self.user}'

    class Meta:
        verbose_name = _('profile')
        verbose_name_plural = _('profiles')

    def save(self, *args, **kwargs):
        if self.__avatar != self.avatar:
            file = f'{MEDIA_URL}{self.__avatar}'
            if os.path.isfile(f'{file}'):
                os.remove(file)
        super().save(*args, **kwargs)

    def delete(self, using=None, keep_parents=False):
        file = f'{MEDIA_URL}{self.avatar}'
        if os.path.isfile(f'{file}'):
            os.remove(file)
        return super().delete(using, keep_parents)

    @staticmethod
    @receiver(models.signals.post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created and sender == User and kwargs:
            UserProfile.objects.create(user=instance)
