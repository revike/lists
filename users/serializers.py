from django.db.models import Q
from rest_framework import serializers
from rest_framework.exceptions import NotAuthenticated, AuthenticationFailed
from rest_framework_simplejwt.serializers import TokenRefreshSerializer, TokenObtainPairSerializer

from base.serializers import Base64FileField
from config.settings import SIMPLE_JWT, BACK_URL, MEDIA_URL
from users.serializer_methods import get_active_list_method
from users.models import User, UserProfile
from users.validators import get_validate_serializer_password


class RegisterSerializer(serializers.ModelSerializer):
    """Register Serializer"""
    password = serializers.CharField(write_only=True)

    class Meta:
        model = User
        fields = ['id', 'email', 'username', 'password', 'created', 'updated']

    def validate(self, attrs):
        data = super().validate(attrs)
        return get_validate_serializer_password(data)


class UserLoginSerializer(TokenObtainPairSerializer):
    """User Login Serializer"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = None

    def validate(self, attrs):

        authenticate_kwargs = {
            self.username_field: attrs[self.username_field],
            'password': attrs['password'],
        }
        try:
            authenticate_kwargs['request'] = self.context['request']
        except KeyError:
            pass

        email = attrs.get('email')
        self.user = User.objects.filter(
            Q(Q(email=email) | Q(username=email)) & Q(is_active=True) & Q(is_verified=True)).first()
        if not self.user:
            raise AuthenticationFailed()
        attrs['email'] = self.user.email
        data = super().validate(attrs)
        return data


class TokenRefreshCustomSerializer(TokenRefreshSerializer):
    """Token Custom Serializer"""
    refresh = None

    def validate(self, attrs):
        request = self.context.get('request')
        cookies = request.COOKIES
        refresh_token = cookies.get('refresh', None)
        if refresh_token is None:
            refresh_token = attrs.get('refresh', None)
        if refresh_token:
            refresh = self.token_class(refresh_token)
            data = {'access': str(refresh.access_token)}
            if SIMPLE_JWT['ROTATE_REFRESH_TOKENS']:
                if SIMPLE_JWT['BLACKLIST_AFTER_ROTATION']:
                    try:
                        refresh.blacklist()
                    except AttributeError:
                        pass
                refresh.set_jti()
                refresh.set_exp()
                refresh.set_iat()
                data['refresh'] = str(refresh)
            return data
        raise NotAuthenticated()


class ProfileForListSerializer(serializers.ModelSerializer):
    """Profile For List Serializer"""

    class Meta:
        model = UserProfile
        fields = ['id', 'avatar']


class UserForListSerializer(serializers.ModelSerializer):
    """User for Product List Serializer"""
    profile = ProfileForListSerializer(read_only=True)

    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'first_name', 'last_name', 'profile', 'created', 'updated']


class UserNoReactionSerializer(serializers.ModelSerializer):
    """User for ProductList No Reaction Invitation"""

    class Meta:
        model = User
        fields = ['id', 'username', 'email', 'first_name', 'last_name', 'created', 'updated']


class ProfileSerializer(serializers.ModelSerializer):
    """Profile Serializer"""
    avatar = Base64FileField(required=False, help_text='data:image/jpg;base64,/9j/4A...', allow_null=True)

    class Meta:
        model = UserProfile
        fields = ['gender', 'birthday', 'phone', 'avatar']

    def to_representation(self, instance):
        to_representation = super().to_representation(instance)
        to_representation['avatar'] = f'{BACK_URL}/{MEDIA_URL}{instance.avatar}' if instance.avatar else None
        return to_representation


class UserProfileDetailSerializer(serializers.ModelSerializer):
    """User Profile Detail Serializer"""
    profile = ProfileSerializer()
    active_list = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = User
        fields = ['id', 'email', 'username', 'first_name', 'last_name',
                  'delay', 'active_list', 'profile', 'created', 'updated']

    def get_active_list(self, instance: User) -> int:
        return get_active_list_method(self, instance)


class UserProfileUpdateSerializer(serializers.ModelSerializer):
    """User Profile Update Serializer"""
    active_list = serializers.SerializerMethodField(read_only=True)
    profile = ProfileSerializer()

    class Meta:
        model = User
        fields = ['id', 'email', 'username', 'first_name', 'last_name',
                  'delay', 'active_list', 'profile', 'created', 'updated']

    def get_active_list(self, instance: User) -> int:
        return get_active_list_method(self, instance)

    def update(self, instance, validated_data):
        profile = validated_data.get('profile')
        if profile:
            profile_data = validated_data.pop('profile')
            ProfileSerializer().update(instance.profile, profile_data)
        return super().update(instance, validated_data)


class ListDetailUserSerializer(serializers.ModelSerializer):
    """List Detail Users Serializer"""
