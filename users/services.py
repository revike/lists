import datetime

import pytz
from rest_framework.response import Response
from rest_framework_simplejwt.tokens import RefreshToken

from config.settings import TIME_ZONE, SIMPLE_JWT, SECURE_HTTP, SAME_SITE, COOKIE_DOMAIN


def set_token_cookie(response: Response, refresh_token: RefreshToken.token_type = None) -> Response:
    """Set tokens"""
    data = response.data
    if not refresh_token:
        refresh_token = data.get('refresh')
    datetime_now = datetime.datetime.now().astimezone(pytz.timezone(TIME_ZONE))
    refresh_expires = datetime_now + SIMPLE_JWT['REFRESH_TOKEN_LIFETIME']
    set_cookies(response=response, key='refresh', value=refresh_token, expires=refresh_expires, httponly=True,
                secure=SECURE_HTTP, same_site=SAME_SITE, domain=COOKIE_DOMAIN)
    return response


def set_cookies(response: Response, key: str, value: str | None = None, max_age: datetime.timedelta | None = None,
                expires: datetime.datetime | None = None, path: str = '/', domain: str | None = None,
                secure: bool = False, httponly: bool = False, same_site: str | None = None) -> Response:
    """Set cookies"""
    response.set_cookie(key=key, value=value, max_age=max_age, expires=expires, path=path,
                        domain=domain, secure=secure, httponly=httponly, samesite=same_site)
    return response


def delete_cookies(response: Response, key: str) -> Response:
    """Delete Cookies"""
    response.delete_cookie(key=key, domain=COOKIE_DOMAIN, samesite=SAME_SITE)
    return response
