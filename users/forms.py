from django import forms
from django.contrib.auth.hashers import make_password

from users.models import User


class UserForm(forms.ModelForm):
    """Admin Form"""

    class Meta:
        model = User
        fields = '__all__'

    def clean_password(self):
        user = self.instance
        password_old = user.password
        data = self.cleaned_data
        password = data.get('password')
        if password != password_old:
            password = make_password(password)
        return password
