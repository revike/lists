from django.contrib.auth.hashers import make_password
from django.utils.translation import gettext_lazy as _
from config.settings import PASSWORD_LENGTH
from rest_framework.serializers import ValidationError


def validate_password(value: str) -> str | bool:
    """Validate password"""
    if value:
        if len(value) >= PASSWORD_LENGTH:
            return value
    return False


def get_validate_serializer_password(data: dict) -> dict:
    """Get validate serializer password"""
    if not validate_password(data.get('password')):
        raise ValidationError({'password': [_(f'Your password is not secure')]})
    data['password'] = make_password(data['password'])
    return data
