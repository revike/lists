from django.db.models import Q
from rest_framework import serializers

from product.models import ProductList
from users.models import User


def get_active_list_method(self: serializers.Serializer, instance: User) -> int:
    """Get active list User's"""
    request = self.context.get('request')
    user = request.user
    active_list = instance.active_list
    if not active_list:
        active_list = ProductList.objects.filter(user=user).first()
    else:
        active_user_list = ProductList.objects.filter(
            Q(id=instance.active_list.id) & Q(Q(user=user) | Q(users=user))).distinct()
        if not active_user_list:
            active_list = ProductList.objects.filter(user=user).first()
    return active_list.id if active_list else None
