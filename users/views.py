from django.db.models import Q
from django.utils.translation import gettext_lazy as _
from drf_spectacular.utils import extend_schema
from rest_framework import generics, status
from rest_framework.exceptions import ValidationError, NotFound
from rest_framework.response import Response
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from base.permissions import IsAuthenticatedPermission
from base.services.utils import get_queryset_users
from base.services.websocket_messages import send_websocket_product_list, send_websocket_invitation
from notice.choices import NotificationType
from notice.models import Notice, Notification
from product.models import ProductList
from users.schemas import response_register, responses_login, responses_refresh, responses_logout, responses_profile, \
    responses_profile_update, responses_list_users, responses_no_reaction_users
from users.serializers import RegisterSerializer, UserLoginSerializer, UserProfileDetailSerializer, \
    UserProfileUpdateSerializer, UserForListSerializer
from users.services import set_token_cookie, delete_cookies


class RegisterApiView(generics.CreateAPIView):
    """Register User"""
    serializer_class = RegisterSerializer

    @extend_schema(summary=_('Registration user'), responses=response_register, tags=['register'])
    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def perform_create(self, serializer):
        user = serializer.save()
        Notification.objects.create(notification_type=NotificationType.Register, user=user)


class LoginApiView(TokenObtainPairView):
    """Login"""

    @extend_schema(summary=_('Login'), request=UserLoginSerializer, responses=responses_login, tags=['login'])
    def post(self, request, *args, **kwargs):
        response = super().post(request, *args, **kwargs)
        data = response.data
        set_token_cookie(response)
        access_token = data.get('access')
        response.data = {'access': access_token}
        return response


class RefreshTokenApiView(TokenRefreshView):
    """Refresh Token"""

    @extend_schema(summary=_('Refresh Token'), responses=responses_refresh, tags=['login'])
    def post(self, request, *args, **kwargs):
        response = super().post(request, *args, **kwargs)
        data = response.data
        set_token_cookie(response)
        access_token = data.get('access')
        response.data = {'access': access_token}
        return response


class LogoutApiView(generics.CreateAPIView):
    """Logout"""

    @extend_schema(summary=_('Logout'), responses=responses_logout, tags=['login'])
    def post(self, request, *args, **kwargs):
        response = Response({'detail': 'logout'}, status=status.HTTP_200_OK)
        delete_cookies(response, 'refresh')
        return response


class UserProfileAPIView(generics.RetrieveAPIView):
    """User Profile"""
    permission_classes = [IsAuthenticatedPermission]
    serializer_class = UserProfileDetailSerializer

    @extend_schema(summary=_('Profile'), responses=responses_profile, tags=['user'])
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_object(self):
        return self.request.user


class UserProfileUpdateAPIView(generics.UpdateAPIView):
    """User Profile Update"""
    permission_classes = [IsAuthenticatedPermission]
    serializer_class = UserProfileUpdateSerializer
    http_method_names = ['patch']

    @extend_schema(summary=_('Profile update'), responses=responses_profile_update, tags=['user'])
    def patch(self, request, *args, **kwargs):
        return super().patch(request, *args, **kwargs)

    def get_object(self):
        return self.request.user

    def perform_update(self, serializer):
        user = serializer.save()
        product_lists = ProductList.objects.filter(Q(Q(user=user) | Q(users=user))).distinct().select_related()
        for product_list in product_lists:
            users = get_queryset_users(product_list)
            send_websocket_product_list(users, product_list.id)
        notices = Notice.objects.filter(sender=user, is_reaction=False)
        for notice in notices:
            send_websocket_invitation(notice.recipient)


class ListUserAPIView(generics.ListAPIView):
    """List detail Users"""
    permission_classes = [IsAuthenticatedPermission]
    serializer_class = UserForListSerializer

    @extend_schema(summary=_('List users'), responses=responses_list_users, tags=['product list'])
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        product_list_id = self.kwargs.get('product_list_id')
        user = self.request.user
        user_filter = Q(user=user) | Q(users=user)
        product_lists = ProductList.objects.filter(Q(id=product_list_id) & Q(user_filter)).select_related()
        if product_lists:
            product_list = product_lists.first()
            if product_list.user == user:
                return product_list.users.all().exclude(id=user.id)
            return product_list.users.filter(id=user.id)
        raise NotFound({'product_list': _('not found')})


class ListNoReactionUserAPIView(generics.ListAPIView):
    """List detail No Reaction Users"""
    permission_classes = [IsAuthenticatedPermission]
    serializer_class = UserForListSerializer

    @extend_schema(summary=_('List no reaction users'), responses=responses_no_reaction_users, tags=['product list'])
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        product_list_id = self.kwargs.get('product_list_id')
        user = self.request.user
        user_filter = Q(user=user) | Q(users=user)
        product_lists = ProductList.objects.filter(Q(id=product_list_id) & Q(user_filter)).select_related()
        if product_lists:
            product_list = product_lists.first()
            if product_list.user == user:
                notices = Notice.objects.filter(
                    sender=user, is_reaction=False, product_list=product_list).select_related('recipient__profile')
                recipients = (notice.recipient for notice in notices)
                return recipients
        raise ValidationError({'users no reaction': [_('No access')]})
