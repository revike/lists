from django.urls import reverse, resolve
from rest_framework import status

from base.tests import BaseTestCase
from notice.models import Notice
from users import views
from users.models import User


class TestAuthCase(BaseTestCase):
    """Tests Users"""

    def setUp(self):
        super().setUp()

    def test_register(self):
        """Test register"""
        data = {
            'username': 'string',
            'email': 'user_register@example.com',
            'password': 'string',
        }
        url, view = reverse('user:register'), views.RegisterApiView
        response = self.make_post(url, None, data, status.HTTP_201_CREATED)
        user_id = response.get('id')
        user = User.objects.get(id=user_id)
        self.assertEqual(user.id, user_id)
        self.assertEqual(response, self.get_serialized_data(user, view.serializer_class))
        self.assertEqual(resolve(url).func.view_class, view)

    def test_login(self):
        """Test Login"""
        data = {'email': self.user.email, 'password': self.password}
        url, view = reverse('user:login'), views.LoginApiView
        response = self.make_post(url, None, data, status.HTTP_200_OK)
        self.assertEqual(response.get('detail', None), None)
        self.assertNotEqual(response.get('access', None), None)
        self.user.is_verified = False
        self.user.save()
        response = self.make_post(url, None, data, status.HTTP_401_UNAUTHORIZED)
        self.assertNotEqual(response.get('detail'), None)
        self.assertEqual(resolve(url).func.view_class, view)

    def test_user_profile(self):
        """Test User Profile"""
        url, view = reverse('user:profile'), views.UserProfileAPIView
        self.make_get(url, None, status_code=status.HTTP_401_UNAUTHORIZED)
        response = self.make_get(url, self.user.email)
        self.assertEqual(response.get('id'), self.user.id)
        self.assertEqual(resolve(url).func.view_class, view)

    def test_profile_update(self):
        """Test profile update"""
        username, first_name, phone = self.user.username, self.user.first_name, self.user.profile.phone
        url, view = reverse('user:profile_update'), views.UserProfileUpdateAPIView
        self.make_patch(url, None, {}, status_code=status.HTTP_401_UNAUTHORIZED)
        data = {
            'username': f'new_{self.user.username}',
            'profile': {'phone': '+79999999999'},
            'first_name': f'new_{self.user.first_name}'
        }
        response = self.make_patch(url, self.user.email, data, status_code=status.HTTP_200_OK)
        self.assertNotEqual(response['username'], username)
        self.assertNotEqual(response['profile']['phone'], phone)
        user_id = response.get('id')
        user = User.objects.get(id=user_id)
        new_username, new_phone, new_first_name = user.username, user.profile.phone, user.first_name
        self.assertNotEqual(username, new_username)
        self.assertNotEqual(phone, new_phone)
        self.assertNotEqual(first_name, new_first_name)
        self.assertEqual(resolve(url).func.view_class, view)

    def test_list_users(self):
        """Test list users"""
        users = self.pl.users.all().exclude(id=self.user.id)
        users_user = self.pl.users.filter(id=self.user2.id)
        url, view = reverse('user:list_users', kwargs={'product_list_id': self.pl.id}), views.ListUserAPIView
        self.make_get(url, None, status_code=status.HTTP_401_UNAUTHORIZED)
        response = self.make_get(url, self.user.email)
        self.assertEqual(len(users), len(response))
        response = self.make_get(url, self.user2.email)
        self.assertEqual(len(users_user), len(response))
        self.assertEqual(resolve(url).func.view_class, view)

    def test_list_no_reaction(self):
        """Test list no reaction users"""
        notices = Notice.objects.filter(product_list=self.pl.id, is_reaction=False, sender=self.user)
        url = reverse('user:list_no_reaction', kwargs={'product_list_id': self.pl.id})
        view = views.ListNoReactionUserAPIView
        self.make_get(url, None, status_code=status.HTTP_401_UNAUTHORIZED)
        response = self.make_get(url, self.user.email)
        self.assertEqual(len(notices), len(response))
        self.make_get(url, self.user2.email, status_code=status.HTTP_400_BAD_REQUEST)
        self.assertEqual(resolve(url).func.view_class, view)
