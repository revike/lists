# Generated by Django 5.0.1 on 2024-03-06 13:44

import base.services.upload_files
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0005_user_created_user_updated'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='avatar',
            field=models.ImageField(blank=True, null=True, upload_to=base.services.upload_files.upload_to_avatar, verbose_name='avatar'),
        ),
    ]
