from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from users.views import RegisterApiView, LoginApiView, RefreshTokenApiView, LogoutApiView, UserProfileAPIView, \
    UserProfileUpdateAPIView, ListUserAPIView, ListNoReactionUserAPIView

app_name = 'users'

urlpatterns = [
    path('register/', RegisterApiView.as_view(), name='register'),
    path('login/', LoginApiView.as_view(), name='login'),
    path('token/refresh/', RefreshTokenApiView.as_view(), name='refresh'),
    path('logout/', csrf_exempt(LogoutApiView.as_view()), name='logout'),

    path('profile/', UserProfileAPIView.as_view(), name='profile'),
    path('profile/update/', UserProfileUpdateAPIView.as_view(), name='profile_update'),

    path('list/<int:product_list_id>/users/', ListUserAPIView.as_view(), name='list_users'),
    path('list/<int:product_list_id>/no/reaction/', ListNoReactionUserAPIView.as_view(), name='list_no_reaction'),
]
