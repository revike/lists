from django.db import models
from django.utils.translation import gettext_lazy as _


class GenderChoice(models.TextChoices):
    """Gender Choices"""
    MR = 'mr', _('Mr')
    MRS = 'mrs', _('Mrs')
    NA = 'n/a', _('N/A')
