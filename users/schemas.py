from rest_framework import status

from base.schemas import base_responses
from users.serializers import RegisterSerializer, TokenRefreshCustomSerializer, UserProfileDetailSerializer, \
    UserProfileUpdateSerializer, UserForListSerializer

response_register = base_responses.copy()
response_register[status.HTTP_201_CREATED] = RegisterSerializer
del response_register[status.HTTP_401_UNAUTHORIZED]
del response_register[status.HTTP_404_NOT_FOUND]
del response_register[status.HTTP_429_TOO_MANY_REQUESTS]

responses_login = base_responses.copy()
responses_login[status.HTTP_200_OK] = TokenRefreshCustomSerializer
del responses_login[status.HTTP_404_NOT_FOUND]

responses_refresh = base_responses.copy()
responses_refresh[status.HTTP_200_OK] = TokenRefreshCustomSerializer
del responses_refresh[status.HTTP_404_NOT_FOUND]

responses_logout = {
    status.HTTP_200_OK: base_responses[status.HTTP_401_UNAUTHORIZED],
    status.HTTP_405_METHOD_NOT_ALLOWED: base_responses[status.HTTP_405_METHOD_NOT_ALLOWED]
}

responses_profile = base_responses.copy()
responses_profile[status.HTTP_200_OK] = UserProfileDetailSerializer

responses_profile_update = base_responses.copy()
responses_profile_update[status.HTTP_200_OK] = UserProfileUpdateSerializer

responses_list_users = base_responses.copy()
responses_list_users[status.HTTP_200_OK] = UserForListSerializer
del responses_list_users[status.HTTP_404_NOT_FOUND]

responses_no_reaction_users = base_responses.copy()
responses_no_reaction_users[status.HTTP_200_OK] = UserForListSerializer
del responses_no_reaction_users[status.HTTP_404_NOT_FOUND]
