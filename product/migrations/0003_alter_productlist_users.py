# Generated by Django 5.0.2 on 2024-02-27 20:51

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0002_productlist_users'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.AlterField(
            model_name='productlist',
            name='users',
            field=models.ManyToManyField(blank=True, related_name='list_users', to=settings.AUTH_USER_MODEL, verbose_name='users'),
        ),
    ]
