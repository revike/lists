# Generated by Django 5.0.2 on 2024-03-10 12:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0005_alter_product_options'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='productlist',
            options={'ordering': ['created'], 'verbose_name': 'product list', 'verbose_name_plural': 'product lists'},
        ),
        migrations.AddField(
            model_name='product',
            name='type_volume',
            field=models.CharField(choices=[('gr', 'gr'), ('pcs', 'pcs')], default='pcs', max_length=3, verbose_name='type volume'),
        ),
    ]
