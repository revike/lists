import os
from decimal import Decimal

from django.core.validators import MinValueValidator
from django.db import models
from django.dispatch import receiver
from django.utils.translation import gettext_lazy as _

from base.choices import UnitChoice
from base.models import BaseModel, NULLABLE
from base.services.upload_files import upload_to_icon
from base.services.utils import get_queryset_users
from base.services.websocket_messages import send_websocket_product_list
from config.settings import MEDIA_URL
from users.models import User


class Tag(BaseModel):
    """Tag"""
    title = models.CharField(_('title'), max_length=128)

    class Meta:
        verbose_name = _('tag')
        verbose_name_plural = _('tags')

    def __str__(self):
        return f'{self.title}'


class Category(BaseModel):
    """Category"""
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='categories', verbose_name=_('user'))
    title = models.CharField(_('title'), max_length=128)
    icon = models.ImageField(_('icon'), upload_to=upload_to_icon, **NULLABLE)
    __icon = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__icon = self.icon

    class Meta:
        verbose_name = _('category')
        verbose_name_plural = _('categories')

    def __str__(self):
        return f'{self.title}'

    def get_user_id(self):
        return self.user.id

    def save(self, *args, **kwargs):
        if self.__icon != self.icon:
            file = f'{MEDIA_URL}{self.__icon}'
            if os.path.isfile(f'{file}'):
                os.remove(file)
        super().save(*args, **kwargs)

    def delete(self, using=None, keep_parents=False):
        file = f'{MEDIA_URL}{self.icon}'
        if os.path.isfile(f'{file}'):
            os.remove(file)
        return super().delete(using, keep_parents)


class Product(BaseModel):
    """Product"""
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_products', verbose_name=_('user'))
    product_list = models.ForeignKey('ProductList', on_delete=models.CASCADE, **NULLABLE,
                                     related_name='pl_products', verbose_name=_('product list'))
    category = models.ForeignKey(
        Category, on_delete=models.SET_NULL, **NULLABLE, related_name='products', verbose_name=_('category'))
    tags = models.ManyToManyField(Tag, blank=True, related_name='product_tags', verbose_name=_('tag'))
    title = models.CharField(_('title'), max_length=128)
    ordering = models.PositiveSmallIntegerField(_('ordering'), default=0)
    price = models.DecimalField(_('price'), max_digits=8, validators=[MinValueValidator(Decimal('0.00'))],
                                decimal_places=2, default=0)
    quantity = models.PositiveIntegerField(_('quantity'), default=1, validators=[MinValueValidator(1)])
    unit = models.CharField(_('unit'), max_length=6, choices=UnitChoice.choices, default=UnitChoice.PCS)
    value = models.BooleanField(_('value'), default=False)

    class Meta:
        verbose_name = _('product')
        verbose_name_plural = _('products')
        ordering = ['ordering', 'id']

    def __str__(self):
        return f'{self.title}'


class ProductList(BaseModel):
    """Product List"""
    title = models.CharField(_('title'), max_length=128)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='list', verbose_name=_('user'))
    products = models.ManyToManyField(Product, blank=True, related_name='list_products', verbose_name=_('products'))
    users = models.ManyToManyField(User, blank=True, related_name='list_users', verbose_name=_('users'))

    class Meta:
        verbose_name = _('product list')
        verbose_name_plural = _('product lists')
        ordering = ['created']

    @staticmethod
    @receiver(models.signals.post_save, sender=User)
    def create_product_list(sender, instance, created, **kwargs):
        if created and sender == User and kwargs:
            product_list = ProductList.objects.create(user=instance, title=_('My first list'))
            instance.active_list = product_list
            instance.save()

    def __str__(self):
        return f'{self.title}'

    def delete(self, using=None, keep_parents=False):
        count = self.__class__.objects.filter(user=self.user).count()
        if count > 1:
            return super().delete(using, keep_parents)
        self.title = _('My first list')
        self.products.all().delete()
        users = self.users.all()
        self.users.remove(*users)
        self.save()

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        users = get_queryset_users(self)
        product_list_id = self.id
        if product_list_id and users:
            send_websocket_product_list(users, product_list_id)
        products = self.products.all()
        products.update(user=self.user)
