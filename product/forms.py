from django import forms
from django.utils.translation import gettext_lazy as _

from product.models import ProductList


class ProductListForm(forms.ModelForm):
    """Product List Form"""

    class Meta:
        model = ProductList
        fields = '__all__'

    def clean_user(self):
        data = self.cleaned_data
        user = data.get('user')
        if getattr(self.instance, 'user', None):
            user_old = self.instance.user
            if user != user_old:
                if ProductList.objects.filter(user=user_old).count() <= 1:
                    ProductList.objects.create(user=user_old, title=_('my list'))
        return user
