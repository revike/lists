from django.contrib import admin
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _

from base.services.utils import get_queryset_users
from base.services.websocket_messages import send_websocket_product_list
from config.settings import BACK_URL, MEDIA_URL
from product.forms import ProductListForm
from product.models import ProductList, Product, Category, Tag
from product.services import add_product_in_list
from users.models import User


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    """Tag admin"""
    list_display = ['id', 'title', 'created', 'updated']
    list_display_links = list_display
    search_fields = ['title']
    fields = list_display
    readonly_fields = ['id', 'created', 'updated']


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    """Category admin"""
    list_display = ['id', 'title', 'user', 'created', 'updated']
    list_display_links = list_display
    search_fields = ['title', 'user__email', 'user__username']
    fields = list_display + ['icon', 'get_preview_icon']
    readonly_fields = ['id', 'created', 'updated', 'get_preview_icon']

    def get_preview_icon(self, obj):
        if obj.icon:
            return mark_safe(f'<img src="{BACK_URL}/{MEDIA_URL}{obj.icon}" style="max-height: 200px;">')

    get_preview_icon.short_description = _('icon preview')


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    """Product admin"""
    list_display = ['id', 'title', 'user', 'product_list', 'category', 'price', 'created', 'updated']
    list_display_links = list_display
    search_fields = ['title', 'user__email', 'user__username', 'category__title', 'tags__title']
    filter_horizontal = ['tags']
    fields = list_display + ['quantity', 'tags']
    readonly_fields = ['id', 'created', 'updated']
    user_product = None

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'category':
            kwargs['queryset'] = Category.objects.filter(user=self.user_product)
        if db_field.name == 'product_list':
            kwargs['queryset'] = ProductList.objects.filter(user=self.user_product)
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def get_form(self, request, obj=None, change=False, **kwargs):
        if getattr(obj, 'user', None):
            user_form = obj.user
            self.user_product = user_form
        return super().get_form(request, obj, change, **kwargs)


@admin.register(ProductList)
class ProductListAdmin(admin.ModelAdmin):
    """ProductList admin"""
    form = ProductListForm
    list_display = ['id', 'user', 'title', 'created', 'updated']
    list_display_links = list_display
    search_fields = ['user__email', 'user__username', 'title', 'users__email', 'users__username']
    filter_horizontal = ['products', 'users']
    fields = list_display + ['users', 'products']
    readonly_fields = ['id', 'created', 'updated']
    user_product = None

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.name == 'products':
            products = Product.objects.filter(user=self.user_product).distinct()
            if products:
                kwargs['queryset'] = products
            self.user_product = None
        return super().formfield_for_manytomany(db_field, request, **kwargs)

    def get_form(self, request, obj=None, change=False, **kwargs):
        if getattr(obj, 'user', None):
            self.user_product = obj.user
        return super().get_form(request, obj, change, **kwargs)

    def delete_queryset(self, request, queryset):
        for product_list in queryset:
            user_obj = product_list.user
            user = User.objects.filter(id=user_obj.id)
            users = product_list.users.all().exclude(id=user_obj.id)
            users |= user
            send_websocket_product_list(users, product_list.id, sleep=5)
        users_id = set(queryset.values_list('user', flat=True))
        super().delete_queryset(request, queryset)
        for user_id in users_id:
            user = User.objects.filter(id=user_id).first()
            product_list = ProductList.objects.filter(user_id=user).count()
            if not product_list:
                ProductList.objects.create(title='My first list', user=user)

    def response_add(self, request, obj, post_url_continue=None):
        users = get_queryset_users(obj)
        if users:
            send_websocket_product_list(users, obj.id)
        add_product_in_list(obj)
        return super().response_add(request, obj, post_url_continue)

    def response_change(self, request, obj):
        users = get_queryset_users(obj)
        if users:
            send_websocket_product_list(users, obj.id)
        add_product_in_list(obj)
        return super().response_change(request, obj)

    def delete_model(self, request, obj):
        users = get_queryset_users(obj)
        if users:
            send_websocket_product_list(users, obj.id, sleep=3)
        return super().delete_model(request, obj)
