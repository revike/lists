import asyncio
import json
from typing import List, Dict

from asgiref.sync import sync_to_async
from django.db.models import Q, Prefetch

from base.consumers import BaseWebsocketConsumer
from product.models import ProductList
from product.serializers import ListDetailSerializer, ListSerializer
from users.models import User
from users.serializers import UserForListSerializer


class ListConsumer(BaseWebsocketConsumer):
    """Product Lists Consumer"""
    serializer_class = ListSerializer

    async def connect(self) -> None:
        """Connect"""
        connect = await super().connect()
        if connect:
            await self.send_json(await self.get_data())

    @property
    def group_name(self) -> str:
        """Getting the group name"""
        return f'group_{self.user.id}'

    @sync_to_async
    def get_product_lists(self) -> List[Dict]:
        """Get products"""
        product_lists_result = []
        product_lists = ProductList.objects.filter(
            Q(user=self.user) | Q(users=self.user)).distinct().select_related('user__profile').prefetch_related(
            Prefetch('users', queryset=User.objects.exclude(id=self.user.id)),
            Prefetch('users__profile'))
        if product_lists:
            product_lists = sorted(product_lists, key=lambda x: x.user == self.user, reverse=True)
            product_lists_serializer = self.serializer_class(product_lists, context=self.get_context, many=True)
            product_lists_result = product_lists_serializer.data
            user_qs = User.objects.filter(id=self.user.id)
            for product_list in product_lists_result:
                if product_list['user']['id'] != self.user.id:
                    product_list['users'] = UserForListSerializer(user_qs, many=True).data
        return product_lists_result

    async def get_data(self) -> List[Dict]:
        """Get data"""
        product_lists = await self.get_product_lists()
        return product_lists

    async def send_product_lists(self, event: dict) -> None:
        if event is not None:
            await asyncio.sleep(2)
            data = await self.get_data()
            await self.send_json(data)

    async def receive_json(self, content: dict, **kwargs) -> None:
        """Answer to message"""
        await self.send_json(await self.get_data())


class ProductConsumer(BaseWebsocketConsumer):
    """Products Consumer"""
    serializer_class = ListDetailSerializer

    async def connect(self) -> None:
        """Connect"""
        connect = await super().connect()
        if connect:
            product_list_id = await self.get_active_list(self.user)
            if product_list_id:
                await self.send_json(await self.get_data_products(product_list_id=product_list_id))

    @property
    def group_name(self) -> str:
        """Getting the group name"""
        return f'group_product_{self.user.id}'

    @sync_to_async
    def get_active_list(self, user: User) -> int | None:
        """Get active list"""
        active_list = user.active_list
        if not active_list:
            active_list = ProductList.objects.filter(user=user).first()
        return active_list.id if active_list else None

    @sync_to_async
    def get_products(self, product_list_id) -> Dict:
        """Get products"""
        products = {'detail': 'The list may have been deleted'}
        product_list = ProductList.objects.filter(
            Q(id=product_list_id) & Q(Q(user=self.user) | Q(users=self.user))).select_related(
            'user__profile').prefetch_related(Prefetch('users', queryset=User.objects.exclude(id=self.user.id)))
        if not product_list:
            product_list = ProductList.objects.filter(user=self.user).select_related('user__profile').prefetch_related(
                Prefetch('users', queryset=User.objects.exclude(id=self.user.id)))
        if product_list:
            result = self.serializer_class(product_list.first(), context=self.get_context)
            products = result.data
            user_qs = User.objects.filter(id=self.user.id)
            if products['user']['id'] != self.user.id:
                products['users'] = UserForListSerializer(user_qs, many=True).data
        return products

    async def get_data_products(self, product_list_id: int) -> Dict:
        """Get data"""
        products = await self.get_products(product_list_id)
        return products

    async def send_update_products(self, event: dict) -> None:
        data = json.loads(event.get('value'))
        if data:
            sleep = data.get('sleep', .2)
            await asyncio.sleep(sleep)
            product_list_id = data.get('product_list_id', None)
            data_products = await self.get_data_products(product_list_id)
            await self.send_json(data_products)

    async def receive_json(self, content: dict, **kwargs) -> None:
        """Answer to message"""
        if content and isinstance(content, dict):
            product_list_id = content.get('product_list_id')
            if product_list_id and isinstance(product_list_id, int):
                await self.send_json(await self.get_data_products(product_list_id))
        else:
            product_list_id = await self.get_active_list(self.user)
            if product_list_id:
                await self.send_json(await self.get_data_products(product_list_id))
