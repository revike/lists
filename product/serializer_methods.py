from rest_framework import serializers

from product.models import ProductList


def get_is_author_method(serializer: serializers.Serializer, instance: ProductList) -> bool:
    """Get check author"""
    request = serializer.context.get('request')
    if request:
        user = request.user
        if user == instance.user:
            return True
        return False
