from django.utils.translation import gettext_lazy as _
from rest_framework import status

from base.schemas import base_responses
from product.serializers import ListSerializer, ListCreateSerializer, ListUpdateSerializer, CategorySerializer, \
    CategoryUpdateSerializer, CategoryCreateSerializer, ProductAddSerializer, ListDetailSerializer, \
    ProductUpdateSerializer, ProductDetailSerializer

# Product lists
responses_product_lists = base_responses.copy()

responses_product_lists[status.HTTP_200_OK] = ListSerializer
del responses_product_lists[status.HTTP_404_NOT_FOUND]

responses_product_list_detail = base_responses.copy()
responses_product_list_detail[status.HTTP_200_OK] = ListDetailSerializer

responses_create_list = base_responses.copy()
responses_create_list[status.HTTP_201_CREATED] = ListCreateSerializer
del responses_create_list[status.HTTP_404_NOT_FOUND]

responses_update_list = base_responses.copy()
responses_update_list[status.HTTP_200_OK] = ListUpdateSerializer

responses_delete_list = base_responses.copy()
responses_delete_list[status.HTTP_204_NO_CONTENT] = None

responses_remove_user = base_responses.copy()
responses_remove_user[status.HTTP_200_OK] = ListCreateSerializer

responses_invitation_cancel = base_responses.copy()
responses_invitation_cancel[status.HTTP_200_OK] = ListSerializer

# Categories
responses_categories = base_responses.copy()
responses_categories[status.HTTP_200_OK] = CategorySerializer
del responses_categories[status.HTTP_404_NOT_FOUND]

responses_category_detail = base_responses.copy()
responses_category_detail[status.HTTP_200_OK] = CategorySerializer

responses_category_create = base_responses.copy()
responses_category_create[status.HTTP_201_CREATED] = CategoryCreateSerializer
del responses_category_create[status.HTTP_404_NOT_FOUND]

responses_category_update = base_responses.copy()
responses_category_update[status.HTTP_200_OK] = CategoryUpdateSerializer

responses_category_delete = base_responses.copy()
responses_category_delete[status.HTTP_204_NO_CONTENT] = None

responses_category_for_product_update = base_responses.copy()
del responses_category_for_product_update[status.HTTP_404_NOT_FOUND]
responses_category_for_product_update[status.HTTP_200_OK] = CategorySerializer
description_category_for_product_update = _("""
***Endpoint for getting a list of categories that the user can change to when editing a product***
""")

# Product
responses_product = base_responses.copy()
responses_product[status.HTTP_200_OK] = ProductDetailSerializer

responses_product_add = base_responses.copy()
del responses_product_add[status.HTTP_404_NOT_FOUND]
responses_product_add[status.HTTP_201_CREATED] = ProductAddSerializer

responses_product_update = base_responses.copy()
responses_product_update[status.HTTP_200_OK] = ProductUpdateSerializer

responses_product_delete = base_responses.copy()
responses_product_delete[status.HTTP_204_NO_CONTENT] = None
