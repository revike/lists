from django.db.models import Q, Prefetch
from django.utils.translation import gettext_lazy as _
from drf_spectacular.utils import extend_schema
from rest_framework import generics, status
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response

from base.permissions import IsAuthenticatedPermission
from base.services.utils import get_queryset_users
from base.services.websocket_messages import send_websocket_product_list, send_websocket_invitation
from notice.choices import NotificationType
from notice.models import Notice, Notification
from notice.utils import get_other_product_list
from product.models import ProductList, Category, Product
from product.schemas import responses_product_lists, responses_create_list, responses_update_list, \
    responses_remove_user, responses_product_list_detail, responses_categories, responses_category_detail, \
    responses_delete_list, responses_category_update, responses_category_delete, responses_category_create, \
    responses_product_add, responses_product_update, responses_product, responses_category_for_product_update, \
    description_category_for_product_update, responses_product_delete, responses_invitation_cancel
from product.serializers import ListSerializer, ListCreateSerializer, ListUpdateSerializer, ListRemoveUserSerializer, \
    CategorySerializer, CategoryUpdateSerializer, CategoryCreateSerializer, ProductAddSerializer, \
    ListDetailSerializer, ProductUpdateSerializer, ProductDetailSerializer, InvitationCancelSerializer
from product.services import check_unique_category, check_unique_list, send_invitation
from users.models import User
from users.serializers import UserForListSerializer


class ListAPIView(generics.ListAPIView):
    """Product List"""
    serializer_class = ListSerializer
    permission_classes = [IsAuthenticatedPermission]

    @extend_schema(summary=_('Product lists'), responses=responses_product_lists, tags=['product list'])
    def get(self, request, *args, **kwargs):
        response = super().get(request, *args, **kwargs)
        product_lists = response.data
        user_qs = User.objects.filter(id=self.request.user.id).select_related('profile')
        for product_list in product_lists:
            if product_list['user']['id'] != self.request.user.id:
                product_list['users'] = UserForListSerializer(user_qs, many=True).data
        return response

    def get_queryset(self):
        user = self.request.user
        queryset = ProductList.objects.filter(
            Q(user=user) | Q(users=user)).distinct().select_related('user', 'user__profile').prefetch_related(
            Prefetch('users', queryset=User.objects.exclude(id=user.id)),
            Prefetch('users__profile'))
        queryset = sorted(queryset, key=lambda x: x.user == user, reverse=True)
        return queryset


class ListDetailAPIView(generics.RetrieveAPIView):
    """Product List Detail"""
    serializer_class = ListDetailSerializer
    permission_classes = [IsAuthenticatedPermission]

    @extend_schema(summary=_('Product list detail'), responses=responses_product_list_detail, tags=['product list'])
    def get(self, request, *args, **kwargs):
        response = super().get(request, *args, **kwargs)
        if response.status_code == 200:
            list_id = self.kwargs.get('pk')
            self.request.user.active_list_id = list_id
            self.request.user.save()
        product_list = response.data
        user_qs = User.objects.filter(id=self.request.user.id)
        if product_list['user']['id'] != self.request.user.id:
            product_list['users'] = UserForListSerializer(user_qs, many=True).data
        return response

    def get_queryset(self):
        user = self.request.user
        product_list_id = self.kwargs.get('pk')
        return ProductList.objects.filter(
            Q(id=product_list_id) & Q(Q(user=user) | Q(users=user))).distinct().select_related(
            'user__profile').prefetch_related(Prefetch('users', queryset=User.objects.exclude(id=user.id)))


class ListCreateAPIView(generics.CreateAPIView):
    """Create Product List"""
    serializer_class = ListCreateSerializer
    permission_classes = [IsAuthenticatedPermission]

    @extend_schema(summary=_('Create Product list'), responses=responses_create_list, tags=['product list'])
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)

    def perform_create(self, serializer):
        check_list = check_unique_list(self.request)
        if not check_list:
            return check_list
        serializer.validated_data['user'] = self.request.user
        product_list = serializer.save()
        send_invitation(self.request.data, product_list.user, product_list)
        users = User.objects.filter(id=self.request.user.id)
        send_websocket_product_list(users, product_list.id, 1)


class ListUpdateAPIView(generics.UpdateAPIView):
    """Product List update"""
    serializer_class = ListUpdateSerializer
    permission_classes = [IsAuthenticatedPermission]
    http_method_names = ['patch']

    @extend_schema(summary=_('Update Product list'), responses=responses_update_list, tags=['product list'])
    def patch(self, request, *args, **kwargs):
        return super().patch(request, *args, **kwargs)

    def perform_update(self, serializer):
        check_list = check_unique_list(self.request)
        if not check_list:
            return check_list
        product_list = serializer.save()
        send_invitation(self.request.data, product_list.user, product_list)
        users = get_queryset_users(product_list)
        if users:
            send_websocket_product_list(users, product_list.id, 1)

    def get_queryset(self):
        user = self.request.user
        product_list_id = self.kwargs.get('pk')
        return ProductList.objects.filter(id=product_list_id, user=user).select_related('user__profile')


class ListDeleteAPIView(generics.DestroyAPIView):
    """Product List Delete"""
    permission_classes = [IsAuthenticatedPermission]

    @extend_schema(summary=_('Delete Product list'), responses=responses_delete_list, tags=['product list'])
    def delete(self, request, *args, **kwargs):
        return super().delete(request, *args, **kwargs)

    def get_queryset(self):
        user = self.request.user
        return ProductList.objects.filter(Q(user=user) | Q(users=user)).distinct().select_related()

    def destroy(self, request, *args, **kwargs):
        user = self.request.user
        instance = self.get_object()
        users = get_queryset_users(instance)
        notices_for_delete = Notice.objects.filter(is_reaction=False, product_list=instance)
        recipients = []
        if users:
            send_websocket_product_list(users, instance.id, sleep=2)
        if user == instance.user:
            notices = notices_for_delete.filter(sender__in=users)
            for notice in notices:
                recipients.append(notice.recipient)
            notices.delete()
            self.perform_destroy(instance)
        else:
            notices = notices_for_delete.filter(recipient=user)
            for notice in notices:
                recipients.append(notice.recipient)
            notices.delete()
            instance.users.remove(user)
            notification_type = NotificationType.CameOutList
            other = get_other_product_list(instance, user)
            Notification.objects.create(notification_type=notification_type, user=instance.user, other=other)
        [send_websocket_invitation(recipient) for recipient in recipients]
        return Response(status=status.HTTP_204_NO_CONTENT)


class ListRemoveUserAPIView(generics.UpdateAPIView):
    """Remove user from Product List"""
    serializer_class = ListRemoveUserSerializer
    permission_classes = [IsAuthenticatedPermission]
    http_method_names = ['patch']

    @extend_schema(summary=_('Remove user from Product list'), responses=responses_remove_user, tags=['product list'])
    def patch(self, request, *args, **kwargs):
        return super().patch(request, *args, **kwargs)

    def get_queryset(self):
        user = self.request.user
        product_list_id = self.kwargs.get('pk')
        data = self.request.data
        user_id = data.get('user_id')
        return ProductList.objects.filter(id=product_list_id, user=user, users__id=user_id).select_related()

    def perform_update(self, serializer):
        data = self.request.data
        user_id = data.get('user_id')
        product_list = serializer.instance
        product_list.users.remove(user_id)

    def update(self, request, *args, **kwargs):
        product_list = self.get_object()
        users = get_queryset_users(product_list)
        response = super().update(request, *args, **kwargs)
        serializer = ListDetailSerializer(product_list)
        response.data = serializer.data
        response.data['is_author'] = True
        if users:
            send_websocket_product_list(users, product_list.id, sleep=2)
        return response


class InvitationCancelAPIView(generics.CreateAPIView):
    """Invitation Cancel"""
    permission_classes = [IsAuthenticatedPermission]
    serializer_class = InvitationCancelSerializer

    @extend_schema(summary=_('Invitation Cancel'), responses=responses_invitation_cancel, tags=['product list'])
    def post(self, request, *args, **kwargs):
        response = super().post(request, *args, **kwargs)
        response.status_code = status.HTTP_200_OK
        product_list_id = self.request.data.get('product_list_id')
        product_list = ProductList.objects.get(id=product_list_id)
        response.data = ListSerializer(product_list).data
        return response

    def perform_create(self, serializer, *args, **kwargs):
        data = serializer.data
        user_id = data.get('user_id')
        product_list_id = data.get('product_list_id')
        user_qs = User.objects.filter(id=user_id)
        if not user_qs:
            raise ValidationError({'user': [_('user not found')]})
        user = user_qs.first()
        product_list = ProductList.objects.filter(id=product_list_id, users=user, user=self.request.user).first()
        if not product_list:
            notice = Notice.objects.filter(recipient=user, product_list_id=product_list_id, is_reaction=False).first()
            if not notice:
                raise ValidationError({'invitation': [_('invitation not found')]})
            notice.delete()
            send_websocket_invitation(user)
        else:
            product_list.users.remove(user)
            send_websocket_product_list(user_qs)


class CategoryAPIView(generics.ListAPIView):
    """Categories"""
    serializer_class = CategorySerializer
    permission_classes = [IsAuthenticatedPermission]

    @extend_schema(summary=_('Categories'), responses=responses_categories, tags=['categories'])
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        user = self.request.user
        return Category.objects.filter(user=user)


class CategoryDetailAPIView(generics.RetrieveAPIView):
    """Category Details"""
    serializer_class = CategorySerializer
    permission_classes = [IsAuthenticatedPermission]

    @extend_schema(summary=_('Category'), responses=responses_category_detail, tags=['categories'])
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        user = self.request.user
        return Category.objects.filter(user=user)


class CategoryCreateAPIView(generics.CreateAPIView):
    """Category Create"""
    serializer_class = CategoryCreateSerializer
    permission_classes = [IsAuthenticatedPermission]

    @extend_schema(summary=_('Category create'), responses=responses_category_create, tags=['categories'])
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)

    def perform_create(self, serializer):
        check_category = check_unique_category(self.request)
        if not check_category:
            return check_category
        serializer.validated_data['user'] = self.request.user
        serializer.save()


class CategoryUpdateAPIView(generics.UpdateAPIView):
    """Category Update"""
    serializer_class = CategoryUpdateSerializer
    permission_classes = [IsAuthenticatedPermission]
    http_method_names = ['patch']

    @extend_schema(summary=_('Category update'), responses=responses_category_update, tags=['categories'])
    def patch(self, request, *args, **kwargs):
        return super().patch(request, *args, **kwargs)

    def get_queryset(self):
        user = self.request.user
        return Category.objects.filter(user=user).select_related()

    def perform_update(self, serializer):
        check_category = check_unique_category(self.request)
        if not check_category:
            return check_category
        category = serializer.save()
        product_lists = ProductList.objects.filter(products__category=category).distinct()
        for product_list in product_lists:
            users = get_queryset_users(product_list)
            if users:
                send_websocket_product_list(users, product_list.id)


class CategoryDeleteAPIView(generics.DestroyAPIView):
    """Category Delete"""
    permission_classes = [IsAuthenticatedPermission]

    @extend_schema(summary=_('Category delete'), responses=responses_category_delete, tags=['categories'])
    def delete(self, request, *args, **kwargs):
        return super().delete(request, *args, **kwargs)

    def get_queryset(self):
        user = self.request.user
        return Category.objects.filter(user=user)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        product_lists = ProductList.objects.filter(products__category=instance).distinct()
        for product_list in product_lists:
            users = get_queryset_users(product_list)
            if users:
                send_websocket_product_list(users, product_list.id)
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)


class CategoryForProductAPIView(generics.ListAPIView):
    """Categories for Product Update"""
    permission_classes = [IsAuthenticatedPermission]
    serializer_class = CategorySerializer
    lookup_field = 'product_id'

    @extend_schema(summary=_('Category for update product'), responses=responses_category_for_product_update,
                   tags=['categories'], description=description_category_for_product_update)
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        product_id = self.kwargs.get('product_id')
        product = Product.objects.filter(id=product_id).first()
        product_list = product.product_list if product else None
        if product_list:
            user = product_list.user
            if user:
                return Category.objects.filter(user=user)


class ProductDetailAPIView(generics.RetrieveAPIView):
    """Product Detail"""
    permission_classes = [IsAuthenticatedPermission]
    serializer_class = ProductDetailSerializer

    @extend_schema(summary=_('Product detail'), responses=responses_product, tags=['product'])
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

    def get_queryset(self):
        user = self.request.user
        return Product.objects.filter(Q(Q(product_list__user=user) | Q(
            product_list__users=user))).distinct().select_related('category', 'product_list')


class ProductAddAPIView(generics.CreateAPIView):
    """Product add"""
    permission_classes = [IsAuthenticatedPermission]
    serializer_class = ProductAddSerializer

    @extend_schema(summary=_('Product add'), responses=responses_product_add, tags=['product'])
    def post(self, request, *args, **kwargs):
        data = self.request.data
        user = self.request.user
        product_list = data.get('product_list', None)
        if isinstance(product_list, str):
            if len(product_list) == 0:
                return Response(
                    {'detail': [{'product_list': _('Incorrect type. Expected pk value, received str.')}]},
                    status=status.HTTP_400_BAD_REQUEST)
        elif isinstance(product_list, int):
            product_list_obj = ProductList.objects.filter(Q(id=product_list) & Q(Q(user=user) | Q(users=user))).first()
            if not product_list_obj:
                return Response(
                    {'detail': [{'product_list': _(f'Invalid pk \"{product_list}\" - object does not exist.')}]},
                    status=status.HTTP_400_BAD_REQUEST)
        elif product_list is None:
            return Response(
                {'detail': [{'product_list': _('This field is required.')}]}, status=status.HTTP_400_BAD_REQUEST)
        return super().post(request, *args, **kwargs)

    def perform_create(self, serializer):
        product = serializer.save()
        product_list = product.product_list if product.product_list else None
        if product_list:
            users = get_queryset_users(product_list)
            if users:
                send_websocket_product_list(users, product_list.id, 1)


class ProductUpdateAPIView(generics.UpdateAPIView):
    """Product Update"""
    permission_classes = [IsAuthenticatedPermission]
    serializer_class = ProductUpdateSerializer
    http_method_names = ['patch']

    @extend_schema(summary=_('Product update'), responses=responses_product_update, tags=['product'])
    def patch(self, request, *args, **kwargs):
        return super().patch(request, *args, **kwargs)

    def perform_update(self, serializer):
        data = self.request.data
        category_id = data.get('category_id')
        if isinstance(category_id, int):
            product_id = self.kwargs.get('pk')
            if product_id:
                product = Product.objects.filter(id=product_id).first()
                if product:
                    product_list = product.product_list if product.product_list else None
                    author = product_list.user if product_list else None
                    if author:
                        category = Category.objects.filter(Q(Q(id=category_id) & Q(Q(user=author))))
                        if not category:
                            message = {'category': [_(f'Category pk \"{category_id}\" - object does not exist')]}
                            raise ValidationError(message)
        product = serializer.save()
        product_list = product.product_list if product.product_list else None
        if product_list:
            users = get_queryset_users(product_list)
            if users:
                send_websocket_product_list(users, product_list.id)

    def get_queryset(self):
        user = self.request.user
        return Product.objects.filter(Q(Q(product_list__user=user) | Q(
            product_list__users=user))).distinct().select_related('category', 'product_list')


class ProductDeleteAPIView(generics.DestroyAPIView):
    """Product Delete"""
    permission_classes = [IsAuthenticatedPermission]

    @extend_schema(summary=_('Product delete'), responses=responses_product_delete, tags=['product'])
    def delete(self, request, *args, **kwargs):
        return super().delete(request, *args, **kwargs)

    def get_queryset(self):
        user = self.request.user
        return Product.objects.filter(
            Q(product_list__user=user) | Q(product_list__users=user)).distinct().select_related()

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        product_list = instance.product_list if instance else None
        if product_list:
            users = get_queryset_users(product_list)
            if users:
                send_websocket_product_list(users, product_list.id)
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)
