from django.urls import resolve
from rest_framework import status
from rest_framework.reverse import reverse

from base.tests import BaseTestCase
from product import views
from product.models import ProductList, Category, Product


class TestProductListCase(BaseTestCase):
    """Tests Product List"""

    def test_product_list(self):
        """Test product list"""
        url, view = reverse('product:lists'), views.ListAPIView
        self.make_get(url, None, status_code=status.HTTP_401_UNAUTHORIZED)
        response = self.make_get(url, self.user.email)
        self.assertEqual(isinstance(response, list), True)
        product_lists = ProductList.objects.filter(user=self.user)
        self.assertEqual(len(response), product_lists.count())
        self.assertEqual(resolve(url).func.view_class, view)

    def test_product_list_detail(self):
        """Test product list detail"""
        url, view = reverse('product:list_detail', kwargs={'pk': self.pl.id}), views.ListDetailAPIView
        self.make_get(url, None, status_code=status.HTTP_401_UNAUTHORIZED)
        response = self.make_get(url, self.user.email)
        self.assertEqual(isinstance(response, dict), True)
        self.assertEqual(response.get('id'), self.pl.id)
        self.assertEqual(resolve(url).func.view_class, view)

    def test_product_list_create(self):
        """Test product list create"""
        data = {'title': 'new list'}
        url, view = reverse('product:list_create'), views.ListCreateAPIView
        self.make_post(url, None, data, status_code=status.HTTP_401_UNAUTHORIZED)
        response = self.make_post(url, self.user.email, data, status_code=status.HTTP_201_CREATED)
        self.assertEqual(isinstance(response, dict), True)
        product_list = ProductList.objects.get(id=response.get('id'))
        self.assertEqual(product_list.id, response.get('id'))
        self.assertEqual(resolve(url).func.view_class, view)

    def test_product_list_update(self):
        pl_title = self.pl.title
        url, view = reverse('product:list_update', kwargs={'pk': self.pl.id}), views.ListUpdateAPIView
        self.make_patch(url, None, {'title': f'{self.pl.title} title'}, status_code=status.HTTP_401_UNAUTHORIZED)
        response = self.make_patch(
            url, self.user.email, {'title': f'{self.pl.title} title'}, status_code=status.HTTP_200_OK)
        self.assertNotEqual(pl_title, response.get('title'))
        self.assertEqual(isinstance(response, dict), True)
        self.assertEqual(resolve(url).func.view_class, view)

    def test_delete_product_list(self):
        """Test delete product list"""
        url, view = reverse('product:list_delete', kwargs={'pk': self.pl.id}), views.ListDeleteAPIView
        self.make_delete(url, None, status_code=status.HTTP_401_UNAUTHORIZED)
        self.make_delete(url, self.user.email, status_code=status.HTTP_204_NO_CONTENT)
        pr_list = ProductList.objects.filter(id=self.pl.id).first()
        self.assertEqual(pr_list, None)

    def test_remove_user_from_product_list(self):
        """Test Remove user from Product List"""
        data = {'user_id': self.user.id}
        url, view = reverse('product:list_remove_user', kwargs={'pk': self.pl.id}), views.ListRemoveUserAPIView
        self.make_patch(url, None, data, status_code=status.HTTP_401_UNAUTHORIZED)
        response = self.make_patch(url, self.user.email, data, status_code=status.HTTP_200_OK)
        self.assertEqual(isinstance(response, dict), True)
        self.assertEqual(resolve(url).func.view_class, view)

    def test_invitation_cancel(self):
        """Test invitation cancel"""
        users_count_start = self.pl.users.all().count()
        data = {
            'user_id': self.user2.id,
            'product_list_id': self.pl.id
        }
        url, view = reverse('product:invitation_cancel'), views.InvitationCancelAPIView
        self.make_post(url, None, data, status_code=status.HTTP_401_UNAUTHORIZED)
        self.make_post(url, self.user.email, data, status_code=status.HTTP_200_OK)
        self.make_post(url, self.user.email, data, status_code=status.HTTP_400_BAD_REQUEST)
        users_count_finish = self.pl.users.all().count()
        self.assertEqual(users_count_start, users_count_finish + 1)
        self.assertEqual(resolve(url).func.view_class, view)


class TestCategoryCase(BaseTestCase):
    """Test Category"""

    def test_categories(self):
        """Test Categories"""
        url, view = reverse('product:categories'), views.CategoryAPIView
        self.make_get(url, None, status_code=status.HTTP_401_UNAUTHORIZED)
        response = self.make_get(url, self.user.email, status_code=status.HTTP_200_OK)
        self.assertEqual(isinstance(response, list), True)
        categories_count = Category.objects.filter(user=self.user)
        self.assertNotEqual(categories_count, len(response))
        self.assertEqual(resolve(url).func.view_class, view)

    def test_category(self):
        """Test Category"""
        url, view = reverse('product:category_detail',
                            kwargs={'pk': self.category_user.id}), views.CategoryDetailAPIView
        self.make_get(url, None, status_code=status.HTTP_401_UNAUTHORIZED)
        response = self.make_get(url, self.user.email, status_code=status.HTTP_200_OK)
        self.assertEqual(isinstance(response, dict), True)
        self.assertEqual(response.get('id'), self.category_user.id)
        self.assertEqual(resolve(url).func.view_class, view)

    def test_category_create(self):
        """Test Category create"""
        data = {'title': f'new {self.category_user.title}'}
        url, view = reverse('product:category_create'), views.CategoryCreateAPIView
        self.make_post(url, None, data, status_code=status.HTTP_401_UNAUTHORIZED)
        response = self.make_post(url, self.user.email, data, status_code=status.HTTP_201_CREATED)
        self.assertEqual(isinstance(response, dict), True)
        self.assertNotEqual(response.get('id'), self.category_user.id)
        self.assertEqual(resolve(url).func.view_class, view)

    def test_category_update(self):
        """Test category update"""
        data = {'title': f'new {self.category_user.title}'}
        url, view = reverse('product:category_update',
                            kwargs={'pk': self.category_user.id}), views.CategoryUpdateAPIView
        self.make_patch(url, None, data, status_code=status.HTTP_401_UNAUTHORIZED)
        self.make_patch(url, self.user2.email, data, status_code=status.HTTP_404_NOT_FOUND)
        response = self.make_patch(url, self.user.email, data, status_code=status.HTTP_200_OK)
        self.assertEqual(isinstance(response, dict), True)
        self.assertNotEqual(response.get('title'), self.category_user.title)
        self.assertEqual(response.get('id'), self.category_user.id)
        self.assertEqual(resolve(url).func.view_class, view)

    def test_category_delete(self):
        """Test Category delete"""
        url, view = reverse('product:category_delete',
                            kwargs={'pk': self.category_user.id}), views.CategoryDeleteAPIView
        self.make_delete(url, None, status_code=status.HTTP_401_UNAUTHORIZED)
        self.make_delete(url, self.user.email, status_code=status.HTTP_204_NO_CONTENT)
        pr_list = Category.objects.filter(id=self.category_user.id).first()
        self.assertEqual(pr_list, None)
        self.assertEqual(resolve(url).func.view_class, view)

    def test_categories_for_update_product(self):
        """Categories for update product"""
        url = reverse('product:categories_for_edit', kwargs={'product_id': self.product.id})
        view = views.CategoryForProductAPIView
        self.make_get(url, None, status_code=status.HTTP_401_UNAUTHORIZED)
        response = self.make_get(url, self.user.email, status_code=status.HTTP_200_OK)
        self.assertEqual(type(response), list)
        self.assertEqual(resolve(url).func.view_class, view)


class TestProductCase(BaseTestCase):
    """Test Product"""

    def test_product(self):
        """Test get product"""
        url, view = reverse('product:product', kwargs={'pk': self.product.id}), views.ProductDetailAPIView
        self.make_get(url, None, status_code=status.HTTP_401_UNAUTHORIZED)
        response = self.make_get(url, self.user.email, status_code=status.HTTP_200_OK)
        self.assertEqual(response.get('id'), self.product.id)
        self.assertEqual(response.get('title'), self.product.title)
        self.assertEqual(resolve(url).func.view_class, view)
        self.assertEqual(resolve(url).func.view_class, view)

    def test_product_add(self):
        """Test product add"""
        product_count_start = Product.objects.all().count()
        pl_products_start = self.pl.products.all().count()
        data = {'title': 'title'}
        url, view = reverse('product:product_add'), views.ProductAddAPIView
        self.make_post(url, None, {}, status_code=status.HTTP_401_UNAUTHORIZED)
        self.make_post(url, self.user.email, data, status_code=status.HTTP_400_BAD_REQUEST)
        data['product_list'] = self.pl.id
        response = self.make_post(url, self.user.email, data, status_code=status.HTTP_201_CREATED)
        product_count_finish = Product.objects.all().count()
        pl_products_finish = self.pl.products.all().count()
        self.assertEqual(product_count_start + 1, product_count_finish)
        self.assertEqual(pl_products_start + 1, pl_products_finish)
        self.assertEqual(response.get('title'), data['title'])
        self.assertEqual(resolve(url).func.view_class, view)

    def test_product_update(self):
        """Test product update"""
        data = {'title': 'new title', 'tags_list': ['tag'], 'category_id': self.category_user.id}
        url, view = reverse('product:product_update', kwargs={'pk': self.product.id}), views.ProductUpdateAPIView
        self.make_patch(url, None, data, status_code=status.HTTP_401_UNAUTHORIZED)
        response = self.make_patch(url, self.user.email, data, status_code=status.HTTP_200_OK)
        self.assertEqual(response.get('id'), self.product.id)
        self.assertNotEqual(response.get('title'), self.product.title)
        self.assertEqual(len(response.get('tags')), len(data['tags_list']))
        self.assertEqual(resolve(url).func.view_class, view)

    def test_product_delete(self):
        """Test Product delete"""
        url, view = reverse('product:product_delete', kwargs={'pk': self.product.id}), views.ProductDeleteAPIView
        self.make_delete(url, None, status_code=status.HTTP_401_UNAUTHORIZED)
        self.make_delete(url, self.user.email)
        product = Product.objects.filter(id=self.product.id).first()
        self.assertEqual(product, None)
        self.assertEqual(resolve(url).func.view_class, view)
