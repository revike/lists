from django.db.models import Q
from django.utils.translation import gettext_lazy as _
from rest_framework.exceptions import ValidationError
from rest_framework.request import Request

from base.services.websocket_messages import send_websocket_invitation
from notice.models import Notice
from product import models as product_models
from users.models import User


def add_product_in_list(product_list: product_models.ProductList) -> None:
    """Add product in ProductList"""
    products = product_list.products.all()
    for product in products:
        products_list_old = product_models.ProductList.objects.filter(products=product).exclude(id=product_list.id)
        if products_list_old:
            [product_list_old.products.remove(product) for product_list_old in products_list_old]
        product.product_list = product_list
        product.save()


def check_validated_data_list(validated_data: dict) -> dict:
    """Check validated data Product List"""
    emails = validated_data.get('emails')
    if isinstance(emails, list):
        validated_data.pop('emails')
    return validated_data


def send_invitation(validated_data: dict, sender: User = None, product_list: product_models.ProductList = None) -> dict:
    """Send an invitation"""
    emails, users = validated_data.get('emails'), []
    if isinstance(emails, list):
        emails = validated_data.pop('emails')
        if sender and product_list:
            emails = [email.strip().lower() for email in emails]
            users = User.objects.filter(Q(email__in=emails) | Q(username__in=emails)).exclude(id=sender.id).distinct()
            user_for_notices = []
            pl_users = product_list.users.all()
            for user in users:
                try:
                    notice, create = Notice.objects.get_or_create(
                        sender=sender, recipient=user, product_list=product_list)
                except Notice.MultipleObjectsReturned:
                    notices = Notice.objects.filter(sender=sender, recipient=user, product_list=product_list)
                    notices.delete()
                    notice, create = Notice.objects.get_or_create(
                        sender=sender, recipient=user, product_list=product_list)
                if not create:
                    if notice.is_reaction and user not in pl_users:
                        notice.is_reaction, notice.is_agree = False, False
                        notice.save()
                        user_for_notices.append(user)
                elif create:
                    user_for_notices.append(user)
            for user_for_notice in user_for_notices:
                send_websocket_invitation(user_for_notice)
    return validated_data


def check_unique_category(request: Request) -> bool | None:
    """Check unique Category"""
    data = request.data
    title = data.get('title')
    user = request.user
    category = product_models.Category.objects.filter(user=user, title=title).first()
    if category:
        raise ValidationError({'category': [_('Category title unique')]})
    return True


def check_unique_list(request: Request) -> bool | None:
    """Check unique Product List"""
    data = request.data
    title = data.get('title')
    user = request.user
    product_list = product_models.ProductList.objects.filter(user=user, title=title).first()
    if product_list:
        raise ValidationError({'product_list': [_('Product list title unique')]})
    return True
