from django.urls import path

from product.views import ListAPIView, ListDetailAPIView, ListCreateAPIView, ListUpdateAPIView, ListRemoveUserAPIView, \
    CategoryAPIView, CategoryDetailAPIView, ListDeleteAPIView, CategoryUpdateAPIView, CategoryDeleteAPIView, \
    CategoryCreateAPIView, ProductAddAPIView, ProductUpdateAPIView, ProductDetailAPIView, CategoryForProductAPIView, \
    ProductDeleteAPIView, InvitationCancelAPIView

app_name = 'product'

urlpatterns = [
    path('lists/', ListAPIView.as_view(), name='lists'),
    path('list/<int:pk>/', ListDetailAPIView.as_view(), name='list_detail'),
    path('list/create/', ListCreateAPIView.as_view(), name='list_create'),
    path('list/update/<int:pk>/', ListUpdateAPIView.as_view(), name='list_update'),
    path('list/delete/<int:pk>/', ListDeleteAPIView.as_view(), name='list_delete'),
    path('list/remove-user/<int:pk>/', ListRemoveUserAPIView.as_view(), name='list_remove_user'),
    path('invitation/cancel/', InvitationCancelAPIView.as_view(), name='invitation_cancel'),

    path('categories/', CategoryAPIView.as_view(), name='categories'),
    path('category/<int:pk>/', CategoryDetailAPIView.as_view(), name='category_detail'),
    path('category/create/', CategoryCreateAPIView.as_view(), name='category_create'),
    path('category/update/<int:pk>/', CategoryUpdateAPIView.as_view(), name='category_update'),
    path('category/delete/<int:pk>/', CategoryDeleteAPIView.as_view(), name='category_delete'),
    path('categories/for/edit/product/<product_id>/', CategoryForProductAPIView.as_view(), name='categories_for_edit'),

    path('<int:pk>/', ProductDetailAPIView.as_view(), name='product'),
    path('add/', ProductAddAPIView.as_view(), name='product_add'),
    path('update/<int:pk>/', ProductUpdateAPIView.as_view(), name='product_update'),
    path('delete/<int:pk>/', ProductDeleteAPIView.as_view(), name='product_delete'),
]
