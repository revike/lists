from rest_framework import serializers

from base.serializers import Base64FileField
from config.settings import BACK_URL, MEDIA_URL
from product.models import ProductList, Product, Category, Tag
from product.serializer_methods import get_is_author_method
from product.services import check_validated_data_list
from users.serializers import UserForListSerializer


class CategorySerializer(serializers.ModelSerializer):
    """Category Serializer"""

    class Meta:
        model = Category
        fields = '__all__'

    def to_representation(self, instance):
        to_representation = super().to_representation(instance)
        to_representation['icon'] = f'{BACK_URL}/{MEDIA_URL}{instance.icon}' if instance.icon else None
        return to_representation


class CategoryCreateSerializer(CategorySerializer):
    """Category Serializer"""
    user = serializers.IntegerField(read_only=True, source='get_user_id')
    icon = Base64FileField(required=False, help_text='data:image/jpg;base64,/9j/4A...', allow_null=True)


class CategoryUpdateSerializer(CategorySerializer):
    """Category Update Serializer"""
    user = serializers.IntegerField(read_only=True, source='get_user_id')
    icon = Base64FileField(required=False, help_text='data:image/jpg;base64,/9j/4A...', allow_null=True)


class TagSerializer(serializers.ModelSerializer):
    """Tag Serializer"""

    class Meta:
        model = Tag
        fields = ['id', 'title']


class ProductSerializer(serializers.ModelSerializer):
    """Product Serializer"""
    category = CategorySerializer()
    tags = TagSerializer(many=True)

    class Meta:
        model = Product
        fields = '__all__'


class ListSerializer(serializers.ModelSerializer):
    """Product List Serializer"""
    user = UserForListSerializer()
    users = UserForListSerializer(read_only=True, many=True)
    is_author = serializers.SerializerMethodField(default=True, read_only=True)

    class Meta:
        model = ProductList
        fields = ['id', 'title', 'user', 'users', 'is_author', 'created', 'updated']

    def get_is_author(self, instance: ProductList) -> bool:
        """Get check author"""
        return get_is_author_method(self, instance)


class ListDetailForProductSerializer(serializers.ModelSerializer):
    """List Detail for Product Serializer"""

    class Meta:
        model = ProductList
        fields = ['id', 'title', 'created', 'updated']


class ListDetailSerializer(serializers.ModelSerializer):
    """List Detail Serializer"""
    products = ProductSerializer(many=True)
    user = UserForListSerializer()
    users = UserForListSerializer(read_only=True, many=True)
    is_author = serializers.SerializerMethodField(default=True, read_only=True)

    class Meta:
        model = ProductList
        fields = ['id', 'title', 'user', 'users', 'products', 'is_author', 'created', 'updated']

    def get_is_author(self, instance: ProductList) -> bool:
        """Get author it"""
        return get_is_author_method(self, instance)


class ListCreateSerializer(serializers.ModelSerializer):
    """Product List Create Serializer"""
    user = UserForListSerializer(required=False, read_only=True)
    emails = serializers.ListField(required=False, write_only=True, help_text='["username", "user@example.com"]')
    is_author = serializers.SerializerMethodField(default=True, read_only=True)

    class Meta:
        model = ProductList
        fields = ['id', 'title', 'user', 'is_author', 'created', 'updated', 'emails']

    def get_is_author(self, instance: ProductList) -> bool:
        """Get author it"""
        return get_is_author_method(self, instance)

    def create(self, validated_data):
        validated_data = check_validated_data_list(validated_data)
        return super().create(validated_data)


class ListUpdateSerializer(ListCreateSerializer):
    """Product List Update Serializer"""
    user = UserForListSerializer(required=False, read_only=True)

    def update(self, instance, validated_data):
        validated_data = check_validated_data_list(validated_data)
        return super().update(instance, validated_data)


class ListRemoveUserSerializer(serializers.Serializer):
    """Product List remove User serializer"""
    user_id = serializers.IntegerField()


class InvitationCancelSerializer(serializers.Serializer):
    """Invitation Cancel Serializer"""
    user_id = serializers.IntegerField(required=True)
    product_list_id = serializers.IntegerField(required=True)


class ProductDetailSerializer(serializers.ModelSerializer):
    """Product Detail Serializer"""
    category = CategorySerializer()
    tags = TagSerializer(many=True)
    product_list = ListDetailForProductSerializer()

    class Meta:
        model = Product
        fields = ['id', 'title', 'product_list', 'price', 'quantity', 'unit',
                  'ordering', 'category', 'value', 'tags', 'created', 'updated']


class ProductAddSerializer(serializers.ModelSerializer):
    """Product add Serializer"""
    tags_list = serializers.ListField(required=False, write_only=True, help_text='["username", "user@example.com"]')
    value = serializers.BooleanField(read_only=True)
    tags = TagSerializer(required=False, read_only=True, many=True)

    class Meta:
        model = Product
        fields = ['id', 'title', 'product_list', 'price', 'quantity', 'unit',
                  'ordering', 'category', 'tags_list', 'value', 'tags', 'created', 'updated']

    def create(self, validated_data):
        tags, tags_list = validated_data.get('tags_list'), []
        if isinstance(tags, list):
            tags_list = validated_data.pop('tags_list')
        request = self.context.get('request')
        user = request.user
        validated_data['user'] = user
        product_list = validated_data.get('product_list')
        if product_list:
            tags_for_add = []
            for tag in tags_list:
                tag_model, _ = Tag.objects.get_or_create(title=f'{tag}')
                tags_for_add.append(tag_model)
            validated_data.pop('user')
            product = Product.objects.create(**validated_data, user=product_list.user)
            if tags_for_add:
                product.tags.add(*tags_for_add)
            product_list.products.add(product)
            return product
        return super().create(validated_data)


class ProductUpdateSerializer(serializers.ModelSerializer):
    """Product Update Serializer"""
    category = CategorySerializer(read_only=True)
    tags = TagSerializer(many=True, read_only=True)
    tags_list = serializers.ListField(write_only=True, help_text='["username", "user@example.com"]')
    category_id = serializers.IntegerField(write_only=True, allow_null=True)

    class Meta:
        model = Product
        fields = ['id', 'title', 'ordering', 'price', 'quantity', 'unit',
                  'value', 'category', 'category_id', 'tags', 'tags_list', 'created', 'updated']

    def update(self, instance, validated_data):
        tags_list = validated_data.get('tags_list')
        if isinstance(tags_list, list):
            tags: list = validated_data.pop('tags_list')
            tag_list = []
            for tag in tags:
                tag_obj, _ = Tag.objects.get_or_create(title=tag)
                tag_list.append(tag_obj)
            instance.tags.remove(*instance.tags.all())
            if tag_list:
                instance.tags.add(*tag_list)
        category_id = validated_data.get('category_id')
        if isinstance(category_id, int):
            category_id = validated_data.pop('category_id')
            instance.category_id = category_id
            instance.save()
        return super().update(instance, validated_data)
